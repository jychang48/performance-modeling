%***************************************************;
%                                                   ;
%  Name                                             ;
%    Driver_Performance.tex                         ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\documentclass[11pt,reqno]{amsproc}
\linespread{1.1}
\usepackage{fullpage}
\usepackage{tikz,everypage}
\usepackage[semicolon,square,authoryear]{natbib}
\numberwithin{equation}{section}
\usepackage{cite}
\usepackage{color}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{wrap fig,lip sum,booktabs}
\usepackage{caption}
\usepackage{comment}
\usepackage{multirow}
\usepackage{rotating}
% \usepackage{algp seudocode}
% \usepackage{algorithm}
% \usepackage{amsbsy}
%
%\usepackage{fullpage}
% \usepackage[scale=0.8]{geometry}
%\usepackage{psfrag}
\usepackage{epstopdf}
\usepackage{enumerate}
\usepackage{enumitem}
%\usepackage{morefloats}
\usepackage[debug=false, colorlinks=true, pdfstartview=FitV, linkcolor=blue, citecolor=blue, urlcolor=blue]{hyperref}
%\usepackage[nomarkers,tablesfirst]{endfloat}
%
\newtheorem{theorem}{Theorem}
\newtheorem{example}[theorem]{Example}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{remark}{Remark}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{proposition}[theorem]{Proposition}

%------------
\newlength{\drop}
\definecolor{amethyst}{rgb}{0.6, 0.4, 0.8}
\definecolor{burgundy}{rgb}{0.5, 0.0, 0.13}

%============================;
%  Front matter: Title etc.  ;
%============================; 
\title{A performance spectrum for parallel computational frameworks that solve PDEs}

\author{\textbf{J.~Chang}, 
\textbf{K.~B.~Nakshatrala}, 
\textbf{M.~G.~Knepley} and 
\textbf{L.~Johnsson}\\
  {\small Correspondence to: \textbf{\emph{e-mail:}} knakshatrala@uh.edu,
  \textbf{\emph{phone:}}+1-713-743-4418}} 

\date{\today}
%\AtBeginDocument{%
%  \AddEverypageHook{%
%    \begin{tikzpicture}[remember picture,overlay]
%      \path (current page.north west) --  (current page.south west) \foreach \i in {1,...,\fakelinenos} { node [pos={(\i-.5)/\fakelinenos}, xshift=\fakelinenoshift, line number style] {\i} }  ;
%    \end{tikzpicture}%
%  }%
%}
%\tikzset{%
%  line numbers/.store in=\fakelinenos,
%  line numbers=60,
%  line number shift/.store in=\fakelinenoshift,
%  line number shift=5mm,
%  line number style/.style={text=gray},
%}
\begin{document}

%================================================================================;
%  Below is CAML logo. All the students working in CAML will use the CAML logo.  ;
%  Do not change the format of the logo paper.                                   ;
%================================================================================;

\begin{titlepage}
    \drop=0.05\textheight
    \centering
    \vspace*{0.3\baselineskip}
    \rule{\textwidth}{1.6pt}\vspace*{-\baselineskip}\vspace*{2pt}
    \rule{\textwidth}{0.4pt}\\[0.3\baselineskip]
    {\LARGE \textbf{\color{burgundy} 
   A performance spectrum for parallel 
   computational \\[0.3\baselineskip] 
   frameworks that solve PDEs}}
   \\[0.3\baselineskip]
   \rule{\textwidth}{0.4pt}\vspace*{-\baselineskip}\vspace{3.2pt}
    \rule{\textwidth}{1.6pt}\\[0.3\baselineskip]
    \scshape
     An e-print of the paper is available 
     on arXiv. \par
%    
    \vspace*{0.3\baselineskip}
%    Authored by \\[0.3\baselineskip]
%    
    {\Large J.~Chang\par}
    {\itshape Graduate Student, University of Houston.}\\[0.3\baselineskip]
%    
{\Large K.~B.~Nakshatrala\par}
    {\itshape Department of Civil \& Environmental Engineering \\
      University of Houston, Texas 77204--4003.\\
    \textbf{phone:} +1-713-743-4418, \textbf{e-mail:} knakshatrala@uh.edu \\
    \textbf{website:} http://www.cive.uh.edu/faculty/nakshatrala}\\[0.3\baselineskip]
%
    {\Large M.~G.~Knepley\par}
    {\itshape Department of Computational 
    and Applied Mathematics, 
      Rice University.}\\[0.3\baselineskip]
%    
    {\Large L.~Johnsson\par}
    {\itshape Department of Computer Science, 
      University of Houston.}
%
\vspace{-0.2in}
%
\begin{figure*}[h]
\subfloat{\includegraphics[scale=0.82]{Figures/Performance_spectrum.pdf}}
\captionsetup{format=hang}
\vspace{-0.12in}
\caption*{\emph{Proposed performance 
spectrum that documents time, intensity 
and rate.}}
\end{figure*}
%
\vfill
{\scshape 2017} \\
{\small Computational \& Applied Mechanics Laboratory} \par
\end{titlepage}

%=========================;
%  Abstract and Keywords  ;
%=========================;
\begin{abstract}
  Important computational physics problems are often
  large-scale in nature, and it is highly desirable
  to have robust and high performing computational
  frameworks that can quickly address these problems.
  However, it is no trivial task to determine whether
  a computational framework is performing efficiently
  or is scalable. 
The aim of this paper is to present various strategies for better understanding the 
performance of any parallel computational frameworks for solving PDEs. 
Important performance issues that negatively impact time-to-solution are 
discussed, and we propose a performance spectrum analysis that can enhance one's 
understanding of critical aforementioned performance issues. As proof of 
concept, we examine commonly used finite element simulation packages and software 
and apply the performance spectrum to quickly analyze the performance and 
scalability across various hardware platforms, software implementations, 
and numerical discretizations. It is shown that the proposed performance spectrum is 
a versatile performance model that is not only extendable to more complex PDEs 
such as hydrostatic ice sheet flow equations, but also useful for understanding
hardware performance in a massively parallel computing environment. 
Potential applications and future extensions of this work are also discussed.
\end{abstract}
% 
\keywords{High Performance Computing, Parallel Computing, Scientific Software, 
Solvers and Preconditioners, Finite Element Methods, Hardware Architecture}

\maketitle

%========================;
%  Include all sections  ;  
%========================;
\input{Sections/S1_Performance_Intro}
\input{Sections/S2_Performance_Issues}
\input{Sections/S3_Performance_Model}
\input{Sections/S4_Performance_Demo}
\input{Sections/S5_Performance_Studies}
\input{Sections/S6_Performance_Conclusions}

%============================;
%  Section: Acknowledgments  ;
%============================;
\section*{ACKNOWLEDGMENTS}
The authors acknowledge Jed Brown (University of Colorado at Boulder) 
for his invaluable thoughts and ideas which inspired the creation of this paper. 
JC and KBN acknowledges partial financial support from the US Department of Energy (DOE) Office
of Science Graduate Student Research (SCGSR) award, which is administered by the 
Oak Ridge Institute for Science and Education for the DOE under Contract DE-SC0014664.
MGK acknowledges partial financial support from the Rice Intel Parallel Computing Center and 
US DOE Contract DE-AC02-06CH11357.
Compute time on the AMD Opteron 2354 and Intel Xeon E5-2680v2 is provided
by the Center for Advanced Computing \& Data Systems (CACDS) at the 
University of Houston. Compute time on the Intel Xeon E5-2695v2, Xeon E5-2698v3, 
and Xeon Phi 7250 is provided by the National Energy Research Scientific Computing Center (NERSC).
The opinions expressed in this paper are those of the authors and 
do not necessarily reflect that of the sponsors. 

%================;
%  Bibliography  ;
%================;
%\bibliographystyle{unsrt}
\bibliographystyle{plainnat}
\bibliography{references}
\end{document}
