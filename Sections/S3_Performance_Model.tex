%***************************************************;
%                                                   ;
%  Name                                             ;
%    S3_Performance_Model.tex                       ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{PROPOSED PERFORMANCE SPECTRUM}
\label{Sec:Model}
The general concept of the performance spectrum model is illustrated by
Figure \ref{Fig:performance_spectrum_outline}. This model
is designed to simultaneously capture both the hardware/architectural
exploitation as well as the algorithmic scalability of a particular 
parallel computational framework. First and foremost, we need the 
time-to-solution since this is the metric of most importance to 
application scientists needing to execute large-scale simulations on
state-of-the-art HPC systems. One may optionally document
the total number of solver iterations needed for convergence. However,
simply knowing the wall-clock time a computational framework needs 
to perform a task tells us little about the computational and algorithmic
efficiency. In order to understand how fast (or slow) a simulation is, we 
need to introduce two more metrics.
%-------------------------------------------;
%  Figure: Performance Model concept        ;
%-------------------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.95]{Figures/Performance_spectrum.pdf}}
\captionsetup{format=hang}
\caption{Proposed
           performance spectrum that documents time, intensity and rate. 
           Intensity is defined as arithmetic intensity (FLOP to TBT ratio) based on 
           cache misses, and rate is defined as degrees-of-freedom solved per second.}
\label{Fig:performance_spectrum_outline}
\end{figure}

\subsection{Intensity}The second metric of interest is the intensity. 
Specifically, we focus on AI. As described in 
\citep{Williams_ACM_2009}, the AI of an algorithm or software is a 
measure that aids in estimating how efficiently the hardware resources and 
capabilities can be utilized. For the five machines listed in Table 
\ref{Tab:S2_HPC}, it is well-known that the limiting factor of performance 
for many applications is the memory-bandwidth. Thus, codes that have a high AI 
have a possibility of reusing data in cache and have lower memory bandwidth 
demands. It should be noted, however, that performance depends on many factors 
such as network latency and file system bandwidth, and the arithmetic intensity 
alone cannot be used to predict performance.

The general formula for the AI is defined as
\begin{align}
\label{Eqn:S3_AI}
\mbox{AI} := \frac{[\mbox{Work}]}{[\mbox{TBT}]},
\end{align}
where [Work] is the total amount of computational effort, typically what one
would refer to as FLOPs. The [TBT] metric is a measure of data movement between
the core/CPU and memory. A cache model is needed in order 
to not only determine the TBT but also to understand what amount of useful 
bandwidth is sustained for a given cache line transfer. One can employ any cache model for
this purpose, such as perfect cache, total number of load and store 
instructions at the core level, traffic at the memory level, or data 
cache misses. Different cache models are useful for interpreting different
behavioral trends, and the choice of cache model depends on the application or
research problem at hand. In this paper, we base [TBT] on the total number of
cache misses and cache line size. The formula for obtaining the TBT 
for the L1, L2, and L3 cache levels is expressed as
\begin{align}
\label{Eqn:S3_TBT}
\mbox{TBT}_{\mbox{Lx}} = \mbox{[Lx misses]}\times\mbox{[Lx line size (byte)]}.
\end{align}
The simplest way to define [Work] is as the total number of floating-point 
operations, denoted FLOPs. Thus the AI based on Lx cache misses is formally written as
\begin{align}
\label{Eqn:S3_AI_complete}
\mbox{AI}_{\mbox{Lx}} = \frac{[\mathrm{FLOPs}]}{\mbox{TBT}_{\mbox{Lx}}}.
\end{align}
If a solver or algorithm experiences a large number of cache misses at the last 
level, memory may impede performance. 

Sometimes the exact TBT of a particular algorithm is not of interest. Instead, 
an application scientist may only care about the relative measure, i.e., whether the 
AI is higher or lower compared to either another algorithm, a different implementation
of the same algorithm, or a different processor. Thus, one may simply look at the 
ratio of FLOPS and cache misses. Equation 
\eqref{Eqn:S3_AI_complete} may be simplified to
\begin{align}
\label{Eqn:S3_AI_simple}
\mbox{AI}_{\mbox{Lx}} = \frac{[\mathrm{FLOPs}]}{[\mbox{Lx misses}]}.
\end{align}
Every machine listed in Table \ref{Tab:S2_HPC} has a cache line size of 64
bytes for all levels of cache. Different CPUs may have different line sizes
and hence a cache miss may imply different memory demands on different
processor architectures. The remainder 
of the paper shall refer to the above formula for estimating the intensity
metric.
\begin{remark}
It should be noted that PAPI's methodology for counting FLOPS may be 
highly inaccurate for the ``Ivybridge'' systems listed in Table \ref{Tab:S2_HPC}.
The hardware counters only count the instructions issued and not the ones
executed or retired. This is paramount for iterative solvers
that rely on SpMV operations because as the codes spend time 
waiting for data to be available from memory, they will reissue the 
floating-point instructions multiple times. These reissues, coupled with 
incomplete filling of a vector unit instruction, can lead to 
overcount factors of up to 10 times. For a more thorough discussion 
on the issue of overcounts, see \citep{weaver2013overcount} and 
the references within. PAPI's FLOP counters 
are disabled on the ``Haswell'' and ``KNL'' processors due to the aforementioned issues
\end{remark}
\begin{remark}The AI can also be counted using other capable software tools and 
methodologies. For example, Intel's Software Development Emulator (SDE) can be used to obtain
FLOP counts \citep{intelSDE,flopSDE}  and Intel\textregistered VTune\texttrademark can be
used to obtain the TBT. This methodology is used in the commonly used Roofline model. 
Alternatively, one can approximate the FLOP count of 
a particular code by inserting counting mechanisms into the code. 
PETSc provides an interface and guidelines for manual FLOP counting, 
and thus FLOP counts for computational frameworks using it can be obtained 
through the performance summary output.
\end{remark}
\begin{remark}The correlation between AI and speedup on a single node 
may not always hold true in a cluster sense (i.e., scaling 
when communication networks are involved). The mechanisms used 
for MPI process info exchanged is very different when the 
processes are on the same node as opposed to on different nodes. 
An application scientist must be fully cognizant of not only
the HPC processor specification but also the network topology as well 
as the interconnect bandwidth and latency.
\end{remark}
\subsection{Rate}
Although $\mbox{AI}_{\mbox{Lx}}$ is useful for comparatively 
estimating the performance a particular parallel framework may attain, 
it does not necessarily aid in predictions of time-to-solution. Consequently, 
this means that AI can easily be ``gamed'' to appear high but the 
code consumes large amounts of wall-clock time. For example, 
small computationally intensive routines such as 
DGEMM \citep{Dongarra} can be inserted to artificially inflate the 
computational demands and increase the AI. Other performance models,
such as the Roofline model, would indicate that this is a favorable trend
while ignoring the fact that more time is spent than necessary. 
This is also why the traditional FLOP rate metric, which can also easily
be gamed, is not helpful either. 
Instead of measuring the total FLOPS executed per second, we measure the total 
degrees-of-freedom solved per second, hence the \emph{Rate} metric needed
to complete the performance spectrum is defined as
\begin{align}
\label{Eqn:S3_rate}
\mbox{Rate}_{1} := \frac{[\mathrm{DOFs}]}{[\mbox{total time (seconds)}]},
\end{align}
where $[\mathrm{DOFs}]$ simply refers to the total number of degrees-of-freedom or 
discrete component-wise equations that need to be solved.
\begin{definition}[Static-scaling]
Equation \eqref{Eqn:S3_rate} is an integral component of what we refer to as 
\emph{static-scaling}, where we increase the problem size but fix the 
concurrency. This is a complete reversal to the classical definition of 
strong-scaling where we fix the problem size but increase the concurrency. 
Static-scaling plots time-to-solution versus the total degrees-of-freedom solved
per second for a variety of problem sizes, so it also has characteristics similar
to the classical definition of weak-scaling where both problem size and concurrency
is increased. 
\end{definition}
%-------------------------------------------;
%  Figure: Performance model rate plot      ;
%-------------------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.85]{Figures/algorithmic_goal.pdf}}
\captionsetup{format=hang}
\caption{Static-scaling plot. By fixing the MPI concurrency and increasing the 
problem size, the rate axis is the degrees-of-freedom solved per second.  
Algorithmic efficiency is achieved when a flat line is observed as the 
problem is scaled up.}
\label{Fig:algorithmic_goal}
\end{figure}

Figure \ref{Fig:algorithmic_goal} contains a pictorial description of a
static-scaling plot and illustrates how to visually interpret the data points. 
A scalable algorithm is $\mathcal{O}(n)$ where $n:=[\mathrm{DOFs}]$ is 
linearly proportional to $[\mbox{total time (seconds)}]$, so it is desirable to 
see a PDE solver maintain a constant rate metric for a wide range of 
problem sizes. The behavior of parallel computational frameworks for solving PDEs is 
not simple because 1) problems too small for a given MPI concurrency 
experience large communication to computation ratios (hence strong-scaling effects) 
and 2) large problems may have unfavorable memory accesses. The static-scaling plots are 
designed to capture both strong-scaling and weak-scaling characteristics and 
can give a good indicator of the ideal range of problem sizes for a given 
MPI concurrency.

The tailing off to the right of the static-scaling plot has two potential reasons.
First, problem size affects how memory is allocated and accessed. Larger problem
sizes may see an increase in memory contention as well as affect the access
pattern to main memory. Thus more time is spent waiting 
on data as opposed to performing calculations. However, another reason 
the tailing off occurs is because solvers for complex PDEs or computational domains may not always be 
$\mathcal{O}(n)$. Suboptimal algorithmic convergence may maintain a consistent
level of hardware utilization but require more iterations and 
FLOPs. To determine whether suboptimal algorithmic convergence plays a role 
in the deterioration of the static-scaling plot, equation \eqref{Eqn:S3_rate}
can be modified as
\begin{align}
\label{Eqn:S3_rate_iterate}
\mbox{Rate}_{2} := \frac{[\mathrm{DOFs}]}{[\mbox{time (seconds)}]\times
[\mbox{no. of solver iterations}]}.
\end{align}
This equation averages out increases in time due to an increase in iteration count.
If a flat line is observed using this metric, then poor algorithmic scalability
did have a negative impact on the static-scaling results.

Alternatively, if one is more interested in the performance gain for each MPI 
process, equation \eqref{Eqn:S3_rate} can also be modified into
\begin{align}
\label{Eqn:S3_rate_mpi}
\mbox{Rate}_{3} := \frac{[\mathrm{DOFs}]}{[\mbox{time (seconds)}]\times[\mbox{no. of MPI 
processes}]}.
\end{align}
This metric presents the average degrees-of-freedom solver per second for each MPI process
or core utilized. 
\subsection{Using the performance spectrum}
The arithmetic intensity and static-scaling components of the spectrum offer a variety of
strategies for interpreting the performance and scalability of any computational framework.
Good performance is achieved when a computational framework achieves low time-to-solution,
high arithmetic intensity, and flat static-scaling lines. The theoretical peak rate of
degrees-of-freedom solved per second could be unknown for a particular algorithm, but the
intensity metric can help us understand whether the static-scaling lines are good by estimating
how well it is efficiently using the available hardware resources. We outline three 
possible ways one could use the performance spectrum model:
\begin{enumerate}
\item \textsf{Hardware limitations:} As mentioned in Section \ref{Sec:Issues}, the
hardware configuration of the compute nodes plays a vital role in the performance spectrum
because different systems have different core counts, frequencies, and memory architectures.
Understanding how PDE solvers behave on different systems is vital for disseminating software 
to the scientific and HPC communities. The different cache sizes listed 
in Table \ref{Tab:S2_HPC} will be reflected in equation \eqref{Eqn:S3_rate}. 
AI$_{\mbox{Lx}}$ is likely to differ on different processors due to 
possible differences in cache sizes and cache policies. Furthermore, different
processors have different clock frequencies, arithmetic capabilities, and memory bandwidth.
%
Moreover, various GNU, Intel, and Cray compilers generate different executables that also
depend on optimization flags used. Compiled code also depend on the data structures 
used as well as code constructs. A particular platform may be better suited for certain 
PDE applications. The performance spectrum model is useful for quickly visualizing and 
comparing the impact of platform characteristics, software, compiler options, and algorithms.
\item \textsf{Software/solver implementation:} There are several software packages suited
for sophisticated finite element simulations such as the C++ based DEAL.II 
package \citep{BangerthHartmannKanschat2007}, the Python based Firedrake Project 
\citep{Rathgeber_ACM_2015}, the Python/C++ based FEniCS Project \citep{alnaes2015fenics},
the C++ based LibMesh \citep{libMeshpaper}, and MOOSE\citep{Gaston2009} projects.
These scientific libraries all use PETSc's linear algebra backend, but they can
also use other packages such as HYPRE \citep{hypre-users-manual} and 
Trilinos/ML \citep{trilinos:overview}. How well can specific solvers or software 
packages solve the same boundary value problem? Algebraic multigrid solvers
have various theoretical approaches and implementation strategies, so it is
entirely possible that certain solver configurations are better suited for 
a particular hardware architecture or PDE. Multigrid solvers for optimization 
remain a difficult research problem, but will be imperative for sustaining a 
high level of computational performance. 
Quick visual representations of the AI and equations solved per second
can certainly guide programmers and scientists in the right direction when designing
or implementing different software and solvers. 
\item \textsf{Numerical discretization:} Finally, various flavors of numerical 
discretizations such as the finite difference, finite element, and finite 
volume methods not only have different orders of mathematical accuracy
but different number of discrete equations to solve for a given mesh. Consider 
the Continuous Galerkin (CG) and Discontinuous Galerkin (DG) finite element methods -- clearly the 
DG method has more degrees-of-freedom since each element has its own copy of a geometric node,
but does that necessarily mean it is more time consuming? For example, if the CG and DG 
elements each take roughly $T$ seconds to attain a solution for the same computational
domain, then the latter element clearly has a higher rate metric because it
has more degrees-of-freedom for a given $h$-size, hence a bigger numerator in 
equation \ref{Eqn:S3_AI}. This is important for
computational scientists and mathematicians that want to compare the convergence rate
of various numerical methods particularly if $p$-refinement studies are involved. 
A cost benefit analysis can be performed when comparing the numerical accuracy vs
computational cost, often quantified using a work-precision diagram~\citep{KnepleyBardhan2015}.
One could also compare the impact finite element discretizations  
have on different geometric elements (e.g., tetrahedra, hexahedra, wedges, etc.). 
The performance of any numerical method depends on the hardware limitations and software
implementations, but this spectrum can be useful for comparing different
and available discretizations and polynomial orders in sophisticated finite element 
simulation packages.
\end{enumerate}
