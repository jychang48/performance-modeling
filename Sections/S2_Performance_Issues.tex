%***************************************************;
%                                                   ;
%  Name                                             ;
%    S2_Performance_issues.tex                      ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{COMMON PERFORMANCE ISSUES}
\label{Sec:Issues}
The performance of any scientific software or algorithm
will depend on a myriad of factors. First and foremost,
good performance depends on efficient and practical 
implementation of the code. Application and domain scientists 
may not be interested in the intricate details of the 
code framework that they did not design, but they must 
still be cognizant of important computational issues that 
may inhibit performance dramatically. 
%
We now briefly highlight some common performances issues 
computational scientist may come across in their line of work:
%-------------------------------------------;
%  Figure: Performance Model concept        ;
%-------------------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.42]{Figures/Streams_overview.pdf}}
\captionsetup{format=hang}
\caption{An overview of the STREAM measurement on two different 
compute nodes. The mapping of MPI bindings has a significant impact on the 
achievable memory bandwidth.}
\label{Fig:streams_overview}
\end{figure}

\begin{itemize}
%---------------------;
%  Core bindings      ;
%---------------------;
\item \textbf{Core/memory bindings}: The simplest way to maximize parallel
performance for MPI applications is to properly enforce MPI process and 
memory bindings. This is particularly 
important for memory bandwidth-limited applications because, on most CPU
architectures, the aggregate core bandwidth exceeds the CPU bandwidth to
memory and it is important to use the CPUs in a multi CPU server in a balanced way. 
Furthermore, if multiple users share a compute node, performance metrics 
can vary greatly as both memory resources and certain levels of cache are 
shared by others. Appropriate mapping 
methodologies for binding ranks to cores is vital for complex hardware architectures 
as well as for complex topological node layouts. Consider the single dual socket 
servers and their respective STREAM Triad benchmark results shown in Figure 
\ref{Fig:streams_overview}. Both the AMD and Intel processors possess two sockets 
where the physical cores are contiguously ordered. However, when 
the MPI processes are placed on alternating sockets, the achievable 
bandwidth is higher for a fixed number of cores by using the memory systems on 
both CPUs. For multi node performance, different binding techniques are 
required -- memory references on a single node are several times faster 
than on a remote node. Process allocation must be carefully done so 
that communication across networks is minimized.
%------------------------------------------------;
%  Table: HPC specification                      ;
%------------------------------------------------;
\begin{table}
\centering
\captionsetup{format=hang}
\caption{Single node specifications from each of the HPC systems used for this study. 
Note that Intel's ``Knights Landing'' (KNL) processor has two different types of memory. \label{Tab:S2_HPC}}
{\tiny
\begin{tabular}{lccccc}
\hline
\multirow{3}{*}{\textbf{Processor}} & \textbf{AMD} & \textbf{Intel} & \textbf{Intel} & \textbf{Intel} & \textbf{Intel} \\
& \textbf{Opteron 2354} & \textbf{Xeon E5-2680v2} & \textbf{Xeon E5-2695v2} & \textbf{Xeon E5-2698v3} & \textbf{Xeon Phi 7250} \\
& \textbf{``Barcelona"} & \textbf{``Ivybridge"} & \textbf{``Ivybridge"} & \textbf{``Haswell"} & \textbf{``Knights Landing"} \\
\hline
Clock rate & 2.2 GHz & 2.8 GHz & 2.4 GHz & 2.3 GHz & 1.4 GHz\\
Year released & 2008 & 2013 & 2013 & 2014 & 2016 \\
Sockets & 2 & 2 & 2 & 2 & 1 \\
Cores/socket & 4 & 10 & 12 & 16 & 68\\
Threads/core & 1 & 1 & 2 & 2 & 4\\
\multirow{2}{*}{L1 cache} & 8$\times$64 KB & 20$\times$32 KB & 24$\times$64 KB & 32$\times$64 KB & 68$\times$64 KB \\
& 2-way associativity & 8-way associativity & 8-way associativity & 8-way associativity & 8-way associativity\\
\multirow{2}{*}{L2 cache} & 8$\times$512 KB & 20$\times$256 KB & 24$\times$256 KB & 32$\times$256 KB & 34$\times$1 MB\\
& 16-way associativity & 8-way associativity & 8-way associativity & 8-way associativity & 16-way associativity\\
\multirow{2}{*}{L3 cache} & 2$\times$2 MB & 2$\times$25 MB & 2$\times$30 MB & 2$\times$40 MB & - \\
& 32-way associativity & 20-way associativity & 20-way associativity & 20-way associativity & -\\
\multirow{2}{*}{Memory type} & \multirow{2}{*}{DDR2-200 MHz} & \multirow{2}{*}{DDR3-1600 MHz} & \multirow{2}{*}{DDR3-1866 MHz} & \multirow{2}{*}{DDR4-2133 MHz} & DDR4-2400 MHz, \\
& & & & & MCDRAM \\
\multirow{2}{*}{Total memory} & \multirow{2}{*}{16 GB} & \multirow{2}{*}{64 GB} & \multirow{2}{*}{64 GB} & \multirow{2}{*}{128 GB} & 96 GB (DDR4), \\
& & & & & 16 GB (MCDRAM) \\
\multirow{2}{*}{Memory channels} & \multirow{2}{*}{4} & \multirow{2}{*}{8} & \multirow{2}{*}{8} & \multirow{2}{*}{8} & 6 (DDR4), \\
& & & & & 8 (MCDRAM) \\
Compiler used & GNU & GNU & Cray & Cray & Cray \\
\multirow{2}{*}{STREAM Triad} & \multirow{2}{*}{10.5 GB/s} & \multirow{2}{*}{64.5 GB/s} & \multirow{2}{*}{102 GB/s} & \multirow{2}{*}{116 GB/s} & 90 GB/s (DDR4),\\
& & & & & 480 GB/s (MCDRAM) \\
\hline
\end{tabular}}
\end{table}
%-------------------------;
%  Hardware architecture  ;
%-------------------------;
\item \textbf{Hardware architecture}: The performance of any benchmark or software 
depends on the hardware architecture. In this paper, we consider five different 
HPC systems with single node specifications listed in Table \ref{Tab:S2_HPC}.
It is evident from the STREAM Triad benchmark that 
different architectures have different levels of achievable memory-bandwidth.
Some of the processors are recent (as of the writing of this paper), 
like the Intel KNL processor whereas others like AMD's ``Barcelona" and 
Intel's ``Ivybridge" processors are older. With increasing complexity of processors
and memory systems, the challenge of good performance of solvers and 
algorithms has become an area of active research. A computational framework 
may solve a PDE efficiently on a laptop or small cluster, but that does not 
mean it will perform efficiently on a supercomputer. Understanding basic 
computer architectural concepts such as pipelining, instruction-level 
parallelism, and cache policies may offer excellent 
guidelines on how to speedup computations by several orders of magnitude. 
For example, Intel's KNL processor has two 512-bit vector units per core and may need
fine-grained parallelism to fully exploit the 68 cores per CPU. If a 
code is not properly vectorized to utilize the 136 vector units capable of 16
floating-point operations per cycle or the 16 GB of onboard
MCDRAM, it is possible that the speedup on this system will not be fully realized, 
and worse yet get outperformed by processors that have faster 
cores. Also, languages such as Python, which are used in some 
sophisticated finite element simulation packages, 
depend on the file system I/O because the interpreter executes system
calls to locate the module and may need to open hundreds of thousands of 
files before the actual computation can begin. Designing and utilizing 
algorithms/languages/compilers that are compatible with recent state-of-the-art 
HPC architectures is paramount \citep{Miller2013405}, otherwise the 
computational performance may be exceedingly poor. 
%-------------------------------------------;
%  Figure: Graph partitioning         ;
%-------------------------------------------;
\begin{figure}[t]
\centering
\subfloat[Default ordering]{\includegraphics[scale=0.25]{Figures/MatNZ1.png}}
\subfloat[Optimized ordering]{\includegraphics[scale=0.25]{Figures/MatNZ2.png}}
\captionsetup{format=hang}
\caption{Assembled sparse matrix where red represents positive 
numbers, blue represents negative numbers, and cyan represents allocated
but unused nonzero entries.}
\label{Fig:domain_decomposition}
\end{figure}
%------------------------;
%  Domain decomposition  ;
%------------------------;
\item \textbf{Domain decomposition}: The global ordering and partitioning of the 
computational elements in a parallel computing environment, particularly for 
problems with unstructured grids, affect both spatial and 
temporal cache locality. Consider the assembled sparse matrices shown in
Figure \ref{Fig:domain_decomposition}. If the nonzero data entries are 
not properly grouped together, the code will invoke expensive cache misses 
and create little opportunity to use data in a cache line and reuse 
data in the cache. Consequently, this 
create serial bottlenecks at the cache/memory levels. Several mesh/graph 
partitioners such as Chaco \citep{hendrickson1995multi}, METIS/ParMETIS 
\citep{METIS}, and PTSCOTCH \citep{chevalier2008pt} are designed
to optimize locality and balance the workload among MPI processes. 
Some graph partitioners use a simple model of communication in seeking
to achieve load balance with minimum communication while others use a
more detailed communication model to better capture the minimum communication
needed by using hypergraphs instead of regular graphs as a basis for partitioning. 
Understanding which type of partitioning to use for the 
PDE problem at hand (e.g., spectral partitioning, geometric partitioning, 
multilevel graph partitioning, etc.) can significantly 
reduce the amount of communication and lead to higher efficiency 
and degree of concurrency.
%----------------------;
%  Solver convergence  ;
%----------------------;
\item \textbf{Solver convergence}: Arguably one of the most important
performance factors to consider for solving PDEs is the convergence rate 
of the solver. Direct methods like Gaussian elimination 
\citep{Grcar2011} as well as its sparse counterparts such as 
MUMPS \citep{amestoy2000mumps} and SuperLU\_DIST \citep{li2003superlu_dist} 
can solve problems in parallel but may have huge memory requirements as the problem
size is scaled up due to fill-in during factorization. Scalable and efficient solvers 
typically rely on the novel combination of iterative solvers and preconditioners.
The Krylov Subspace (KSP) and Scalable Nonlinear Equations Solvers (SNES) features
in the PETSc library coupled with robust preconditioners 
\citep{SmithBjorstadGropp1996,BrandtLivne2011} is a popular methodology
for solving large and complex PDEs. Novel combinations and tuning of solver 
parameters provide powerful and robust frameworks that can accurately 
and quickly converge to a specified residual tolerance, even for complex 
coupled multi-physics problems \citep{Castelletto2016894,bkmms2012,BruneKnepleySmithTu15}. 
Simple preconditioners 
such as Jacobi or Incomplete Lower Upper (ILU(0)) factorization may be 
fast for smaller problems, but the computational cost will soar because 
the number of solver iterations needed with Jacobi or ILU(0) will rapidly grow
with problem size. Scaling up the problem under these choices of preconditioning 
will be extremely time consuming and may not even converge for larger or more
complicated problems. Other more robust preconditioners, like the geometric 
and algebraic multigrid method, might have a more expensive setup time 
for smaller problems but have been demonstrated to maintain relatively 
uniform convergence for larger problems, even those that are nonsymmetric 
and indefinite \citep{bramble1994multigrid,Adams2004}.
\end{itemize}
These important performance issues should not be overlooked when 
analyzing the performance of a parallel computational framework. 
There are also several quick strategies for understanding and identifying 
bottlenecks on a specific HPC system. For example, it is well-known
that SpMV is an operation that is sensitive to the memory-bandwidth. These 
operations have very low AI's which can present itself as a bottleneck 
at the memory level on a single node. 
A simple test one can perform is to run the SpMV operation in parallel, and if 
it does not scale well on a single server in the strong sense, the memory-bandwidth 
is clearly limiting the performance. One can confirm 
this by running some simple vector operations like the vector sum and scalar multiplication to see if
they experience the same scaling issues. In addition, one can test the vector
dot product operation in order to detect problems with the
network interconnect or memory latency issues. The PETSc performance
summary~\citep{petsc-user-ref} provides comprehensive insight into the performance of many 
of these important operations including load balancing. The summary also 
provides information on the functions consuming most of the time. 
However, not all scientific software 
have readily available performance summaries, so a 
performance model amenable to any code implementation is needed to 
help answer common performance questions.

