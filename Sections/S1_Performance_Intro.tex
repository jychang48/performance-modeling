%***************************************************;
%                                                   ;
%  Name                                             ;
%    S1_Performance_Intro.tex                       ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{INTRODUCTION AND MOTIVATION}
\label{Sec:Introduction}
Both efficient algorithms and software performing well on modern computing
systems are crucial to address
current scientific and engineering problems. These
tools are important for bridging the gap between theory and 
real-world data. Such problems often need to tackle field-scale data
sets using parallel computers, parallel algorithms, and programming
tools such as OpenMP \citep{dagum1998openmp} and the Message Passing 
Interface (MPI) \citep{gropp1999using} and cannot be solved on a 
standard laptop or desktop. For example, hydrologists and geophysicists 
need to work with field-scale reservoirs which could span tens of 
kilometers and evolve on time scales of hundreds of 
years. Moreover, such reservoir simulations involve complicated multi-phase 
and multi-component flows which require multiple complex equations to 
be solved accurately and efficiently. Atmospheric and climate modelers
also require state-of-the-art techniques as both data assimilation and
parameter estimation need to be performed quickly on meso-scale and 
global-scale applications. The US Department of Energy has 
invested in the development of several portable and extensible 
scientific software packages like PETSc \citep{petsc-user-ref,petsc-web-page} 
and PFLOTRAN \citep{pflotran-web-page} that can help address 
such important large-scale problems. The time spent developing 
parallel computational frameworks is amortized when application 
scientists employ the packages in their work.

However, it is not always known whether the performance of a particular 
parallel computational framework or software will be satisfactory across a panoply of 
solvers and computing platforms. How can one really tell whether an algorithm 
is performing at its highest level? Is there room for improvement? Answering 
these questions in full is a Herculean task, but questions regarding the 
algorithmic and computational efficiency of scientific tools and 
libraries still need to be answered \citep{keyes2013multiphysics}. Hence,
we need \emph{performance models} which enable us to synthesize 
performance data into an understandable framework. Performance models
can include many metrics of importance such as total floating point 
operations (FLOP), memory usage, inter/intra process/node communication,
memory/cache bandwidth, and cache misses/hits. If not carefully optimized, 
some of the hardware resources can become unnecessary bottlenecks that result in costly and 
inefficient numerical simulations. Modern computer systems are quite
complex and the performance can be difficult to predict with good
accuracy. Conducting large-scale simulations on state-of-the-art 
supercomputers may require hundreds to thousands of hours of compute time, 
so it is highly desirable to have a performance model that can predict 
how a particular parallel computational framework may perform. The 
application or domain scientist may use software that either is
not made in house or is a ``black-box'' tool, and it would be too
time consuming, or impossible if source code is unavailable, to 
dissect the code and analyze the 
design of the subroutines and data structures. It is therefore 
desirable to analyze these codes as a whole.
\subsection{Review of previous works}
We now briefly highlight some useful approaches and models one 
could take to analyze and perhaps improve the performance of 
any parallel computational framework. One of the simplest measures 
one can utilize is the STREAM memory-bandwidth benchmark \citep{streams}. 
This benchmark measures sustainable memory-bandwidth on a single server 
and indicates the number of threads that saturates memory bandwidth.
Memory-bandwidth is an important limitation 
to consider on modern machines \citep{wulf1995hitting,mckee2004reflections,
murphy2007effects}

The Roofline model \citep{Williams_ACM_2009,Lo_roofline} captures peak 
achievable performance on a server taking into account both CPU and 
memory-bandwidth capabilities by introducing the Arithmetic Intensity (AI). The 
AI is simply the measure of the total floating-point operations needed, total
FLOP, over Total Bytes Transferred (TBT).
Higher AI's indicate that the algorithm or computational framework is
more computationally intensive and requires less bandwidth for a given
amount of work. One is free to employ any cache model
when determining the TBT metric for the roofline model. 
For example, scientists have developed a Sparse Matrix-Vector (SpMV) 
multiplications model \citep{Kaushik99towardrealistic} which is based on 
``perfect cache" (i.e., matrices and vectors are loaded and 
stored once from memory). SpMV is an integral part 
of iterative solvers for solving PDEs. It has been 
shown in \citep{Chang_JOMP_2016} that the SpMV ``perfect cache" model
can also be used to accurately predict and understand the hardware 
performance of optimization-based solvers for enforcing discrete 
maximum principles. In \citep{may_ptatin}, the authors employ 
matrix-free iterative methods for Stokes equation, which is needed
for lithospheric dynamic applications. The authors manually count the TBT 
based on source code. The advantage of matrix-free methods is that the sparse
matrix-vector multiplication, which is memory-bandwidth limited, is not explicitly
stored thus bringing the computational frameworks' upper-bound limit 
of the roofline closer to the Theoretical Peak Performance (TPP) region.
TBT can also be determined based on memory level traffic or cache misses. 
The same analysis can be carried out for many-core architectures,
such as Nvidia GPUs and the Intel Xeon Phi ``Knights Landing'' (KNL), 
in \citep{KnepleyTerrel2013,KnepleyRuppTerrel2016}. {\color{red} We note that while no explicit cache model is present in the
analysis, such as the external memory model~\citep{AggarwalVitter1988}, if locality were a large determinant of runtime
we would expect to see increases in computation rate with decreasing problem size. However, in the tests in
Section~\ref{Sec:Studies} we do not see this effect, and conclude that an explicit cache model would be of limited
usefulness in this context.}

For a more thorough analysis of performance, advanced software tools such as
the HPCToolkit \citep{adhianto2010hpctoolkit}, OpenSpeedShop \citep{schulz2008open},
Scalasca \citep{geimer2010scalasca}, and TAU \citep{shende2006tau}
are used by scientific software developers and application scientists alike.
These tools provide in-depth performance analyses 
of scientific codes and can also be used to debug the codes. Many of them
also rely on PAPI \citep{mucci1999papi} 
which use low level hardware counters for important metrics like 
FLOPs, total CPU (central processing unit) cycles, and 
cache misses. These tools have proven to be extremely useful for 
computational scientists in all areas of computational 
physics and can provide a good understanding of the hardware 
performance of any computational framework for solving PDEs.

%==========================================;
%  Subsection:  Organization of the dissertation  ;
%==========================================;
\subsection{Main contributions}
In this paper, we provide a simple and easy-to-use performance
model that can be used in addition to the techniques and tools
mentioned above. Our performance model, which we refer to as a
\emph{performance spectrum}\footnote{We borrowed the terminology ``\emph{performance
    spectrum}'' from Dr.~Jed Brown, which he used in 
  his presentation at 2016 SIAM Parallel Processing
  conference, which was held at Paris, France
  (conference website:~http://www.siam.org/meetings/pp16/).}
%
takes into account time-to-solution, AI based on cache misses, and 
equations solved per second. This model is applicable to any level of 
a scientific code, whether it be the entire computational framework or 
only particular phases or functions such as mesh generation, assembly of a
matrix, or the solver step. \emph{It is important to note that this tool is 
not intended to replace any of the aforementioned performance 
tools or models but to simply augment one's ability to quickly understand 
and diagnose the performance from both the hardware, software, and algorithmic 
stand point.} The main contributions of this paper can be enumerated as follows: 
\begin{enumerate}
\item We outline common issues pertaining to performance,
  ways to identify them, and methods to address them.
\item We present a model called performance spectrum that provides
  an enhanced understanding of the performance and
  scalability of algorithms and software.
\item We demonstrate that the proposed model 
  can be utilized on existing popular software
  packages and solvers.
\item We apply the model to a more complicated and 
  nonlinear PDE and document the parallel performance 
  of the computational framework across HPC machines.
\item We discuss some possible ways in which this
  performance spectrum model can be extended.
\end{enumerate}

The rest of the paper is organized as follows. 
In Section \ref{Sec:Issues}, we outline some of the key performance
issues one may come across when solving PDEs
and how to address some of them. In Section
\ref{Sec:Model}, we propose a model, performance spectrum, which captures 
three critical metrics useful for understanding 
performance and scalability. In Section \ref{Sec:Demo}, we 
demonstrate possible ways one could utilize the proposed model 
by systematically comparing commonly used finite element 
packages and solvers. In Section \ref{Sec:Studies}, 
we extend the model to simulate nonlinear hydrostatic 
ice sheet flow equations. In Section \ref{Sec:Studies2}, we 
run the nonlinear hydrostatic ice sheet flow equations
across multiple compute nodes and study the performance. 
Concluding remarks and possible extensions of this work are 
outlined in Section \ref{Sec:Conclusions}.
%
All the notational conventions employed in this paper
are introduced as needed.
