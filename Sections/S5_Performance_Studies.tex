%***************************************************;
%                                                   ;
%  Name                                             ;
%    S5_Performance_Studies.tex                 ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{CASE STUDY PART 1: SINGLE NODE}
\label{Sec:Studies}
The previous section, which focused entirely on the steady-state diffusion equation, 
covered the basic ways one can utilize the proposed
performance spectrum model to help justify, interpret, or diagnose the 
computational performance of any algorithm, numerical method, or solver for a
particular compute node. In these next two sections, 
we demonstrate that this performance spectrum model 
is also useful for more complicated and nonlinear PDEs. 
We consider PETSc's toy hydrostatic ice sheet 
flow example, based on the work of \citep{brown2013icesheet}, 
with geometric multigrid and apply the performance spectrum to
give us a better understanding of how certain HPC platforms scale.

\subsection{Hydrostatic ice sheet flow equations}
Consider a $[0,10]\mathrm{km} \times [0,10]
\mathrm{km} \times [0,1]\mathrm{km}$ computational ice domain $\Omega \subset \mathbb{R}^3$ lying between
a Lipschitz continuous bed $b(x,y)$ and surface $s(x,y)$. The hydrostatic equations
are obtained from the non-Newtonian Stokes equations where the horizontal 
$x-$ and $y-$ derivatives of velocity in the vertical $z$-direction are small 
and negligible. Denoting the horizontal component of the velocity field by 
$\boldsymbol{u} = (u,v)$ where $u$ and $v$ are parallel to the $x-$ and $y-$ axes
respectively, the governing equations for hydrostatic ice sheet flow is given
by
\begin{subequations}
\label{Eqn:S5_icesheet}
\begin{align}
-\eta\left(\frac{\partial}{\partial x}\left(4\frac{\partial u}{\partial x}+2\frac{\partial v}{\partial y}\right) + 
\frac{\partial}{\partial y}\left(\frac{\partial u}{\partial y}+\frac{\partial v}{\partial x}\right) +  \frac{\partial^2 u}{\partial z^2}\right) + \rho g\frac{\partial s}{\partial x}= 0\quad\mathrm{and}\\
-\eta\left(\frac{\partial}{\partial x}\left(\frac{\partial u}{\partial y}+\frac{\partial v}{\partial x}\right) +
\frac{\partial}{\partial y}\left(2\frac{\partial u}{\partial x}+4\frac{\partial v}{\partial y}\right) +  \frac{\partial^2 y}{\partial z^2}\right) + \rho g\frac{\partial s}{\partial y} = 0,
\end{align}
\end{subequations}
where $\eta$ is the nonlinear effective viscosity expressed by
\begin{align}
\eta(\gamma) = \frac{B}{2}\left(\frac{\epsilon^2}{2}+\gamma\right)^{\frac{1-n}{2n}},
\end{align}
where ice sheet models typically take $n=3$. The hardness parameter is denoted by $B$,
the regularizing strain rate is defined by $\epsilon$, and
the second invariant $\gamma$ is expressed by
\begin{align}
\gamma = \frac{\partial^2u}{\partial x^2}+\frac{\partial^2v}{\partial y^2}
+ \frac{\partial u}{\partial x}\frac{\partial v}{\partial y} + \frac{1}{4}\left(
\frac{\partial u}{\partial y}+\frac{\partial v}{\partial x}\right)^2 + \frac{1}{4}
\left(\frac{\partial^2 u}{\partial x^2}+\frac{\partial^2 v}{\partial x^2}\right).
\end{align}
More information on the theoretical derivation of the above equations can be 
found in \citep{schoof2006icestream,schoof2010thinfilm}. Equation 
\eqref{Eqn:S5_icesheet} is subject to natural boundary conditions at the free surface
and either no-slip or power-law slip conditions with friction parameter
\begin{align}
\beta^2(\gamma_b) = \beta^2_0\left(\frac{\epsilon^2_b}{2}+\gamma_b\right)^{\frac{m-1}{2}},
\end{align}
where $\gamma_b=\frac{1}{2}\left(u^2+v^2\right)$, $\epsilon_b$ is regularizing velocity,
$\beta^2_0$ is a ``low-speed" reference friction, and $m\in (0,1]$ is the slip exponent.
\subsection{Problem setup}
The hydrostatic ice sheet flow equation is discretized using hexahedron Q1 
finite elements on a structured grid and Figure \ref{Fig:iceflow_example}
contains the corresponding solution. Details concerning the theoretical 
derivation of the variational formulation as well as the parameters used 
for the boundary value problem can be found in \citep{brown2013icesheet}.
Since this example problem is written entirely with PETSc routines and function calls,
the [FLOPs] metric in equation \eqref{Eqn:S3_AI_simple} is determined 
using PETSc's manual FLOP counts instead of hardware FLOP counters. This is
particularly useful if a thorough comparative study on PETSc's eclectic suite
of linear algebra solvers for a particular PDE were to be conducted.

For this problem, we begin with an initial coarse grid size and successively refine
the grid $N$ times until we get the desired problem size and numerical accuracy.
The ``fine grids" produced from this element-wise refinement are solved using the geometric
multigrid technique, whereas the initial coarse grid is solved using algebraic multigrid.
The assembled Jacobian employs block AIJ format (better known as the compressed
sparse row format), where the horizontal velocity components are grouped per grid node. 
Since ice-flow is tightly coupled in the vertical direction, parallel domain decomposition 
is specially set up so that grid points in the vertical direction are never distributed
and are always contiguous in memory. The initial grid size must be chosen carefully 
because the mesh partition happens at the coarsest level and may cause load balancing 
issues if the initial grid is not large enough.
\subsection{Results}
%----------------------------------------;
%  Figure: Ice sheet flow example        ;
%----------------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.42]{Figures/Icesheet_example.png}}
\captionsetup{format=hang}
\caption{Numerical solution of the velocity vector field for the 
hydrostatic ice sheet flow example.}
\label{Fig:iceflow_example}
\end{figure}
%---------------------;
%  Table: Icesheet 1  ;
%---------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Hydrostatic ice sheet flow for a single node: Mesh information and 
  number of solver iterations needed for an initial 40$\times$40$\times$5 coarse grid.
  KSP and SNES iteration counts may vary depending on the number of MPI processes used.
  \label{Tab:icesheet1}}
  \begin{tabular}{ccccc}
    \hline
    Levels of refinement & Degrees-of-freedom & SNES iterations & KSP iterations \\
    \hline
    0 & 16,000 & 7 & 39 \\
    1 & 115,200 & 8 & 45 \\
    2 & 870,400 & 8 & 44 \\
    3 & 6,758,400 & 8 & 44 \\
    \hline
  \end{tabular}
\end{table}
%-----------------------------------;
%  Figure: Small iceflow intensity  ;
%-----------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow1_intensity.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow single node: L1 arithmetic intensity, based on 
PETSc's manual FLOP counts and L1 cache misses. Note that the
two Ivybridge and KNL nodes are only partially 
saturated.}
\label{Fig:iceflow1_intensity}
\end{figure}

%------------------------------;
%  Figure: Small iceflow rate  ;
%------------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow1_rate.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow single node: Static-scaling. 
Note that the two Ivybridge and KNL nodes are only partially saturated.}
\label{Fig:iceflow1_rate}
\end{figure}

%------------------------------;
%  Figure: Small iceflow rate  ;
%------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow1_rate2.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow single node: Static-scaling per MPI processes. 
Note that the two Ivybridge and KNL nodes are only partially saturated.}
\label{Fig:iceflow1_rate2}
\end{figure}
First, we provide an initial 40$\times$40$\times$5 coarse grid and 
successively refine this grid up to 3 times. All 
five processors from Table \ref{Tab:S2_HPC} are studied, and the KNL
processor is configured to use MCDRAM in flat mode. Each node has a 
different number of available cores so in order to maintain relatively 
consistent mesh partitioning, the Ivybridge and KNL processors will only
utilize 16 and 64 cores, respectively. Table \ref{Tab:icesheet1} presents the
problem size as well as the number of total SNES and KSP iterations needed 
for each level of refinement. Figure \ref{Fig:iceflow1_intensity} 
depicts the AI$_{\mbox{L1}}$ metrics with respect to the overall time-to-solution. 
Each data point has the same coarse grid size but has different levels of grid 
refinement ranging from 0 to 3. The Ivybridge'' and Haswell processors 
have similar AIs and are significantly smaller than their KNL and AMD counterparts. It should be noted
that GNU compilers were used to compile the problem on the AMD Opteron 2354 and 
Intel Xeon E5-2680v2 processors whereas the other three processors used Cray
compilers, which could explain why the AIs between the two Ivybridge processors are slightly 
different. As with the hardware counter examples in the last section, 
the AIs are initially small for the coarser
problems but eventually stabilize if a problem is sufficiently large.
It is interesting to note that the AMD processor is consistently flat for all 
data points, suggesting that the smaller problem sizes selected for this example 
have already approached the AMD processor's achievable peak performance of the
computational/memory resource. On the other hand, the KNL's wider vector 
instruction sets and caches for smaller problems are not fully utilized, 
resulting in low AI's.

The static-scaling plot on each of these compute nodes is shown in Figure 
\ref{Fig:iceflow1_rate}, and Figure \ref{Fig:iceflow1_rate2} depicts static-scaling
based on Rate$_{3}$ from \eqref{Eqn:S3_rate_mpi}. Unsurprisingly, the AMD 
processor is outperformed by all of the Intel processors. Both Haswell and KNL 
have the best performance out of all the systems studied, 
but we again notice that the KNL processor has poor metrics when the grid 
is small. Furthermore, KNL's performance per core is considerably lower as seen
from Figure \ref{Fig:iceflow1_rate2}. There are many
reasons why we noticed such dramatic behavior. First, as we already noted 
from the AI results, the problem has to be sufficiently large in order
to fully utilize the KNL vector instructions. Second, we used 64 of the 68 available
cores on the KNL node, which is at least double the amount of cores  that 
the other systems have. The degrees-of-freedom per MPI is significantly 
smaller so it is possible interprocess communication time affects the
scaling results. 

\section{CASE STUDY PART 2: MULTIPLE NODES}
\label{Sec:Studies2}
The results from every example in this paper thus far behoove us to now 
investigate what happens when more than one compute node is needed
to solve a PDE. Figures \ref{Fig:demo1_speedup} and \ref{Fig:demo2_rate}
from the previous section indicate that the AI$_{\mbox{Lx}}$ metrics
can be used to predict the strong-scaling potential on a single node. 
Our goal is now to investigate if the correlation holds true even across
multiple nodes. To ensure that the problem is sufficiently large 
to distribute to several nodes, we consider an initial 
64$\times$64$\times$6 coarse grid with three levels of refinement 
(21,495,808 degrees-of-freedom). The number of KSP and SNES iterations
needed to solve the problem are 62 and 8, respectively.
%-------------------------;
%  Table: Strong-scaling  ;
%-------------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Hydrostatic ice sheet flow strong-scaling for an 
  initial 64$\times$64$\times$6 coarse grid.
  \label{Tab:icesheet_strong}}
  {\footnotesize
  \begin{tabular}{cccccccccc}
    \hline
    \multirow{2}{*}{Nodes} & \multicolumn{3}{|c}{E5-2695v2 (Ivybridge)} & \multicolumn{3}{|c}{E5-2698v3 (Haswell)} & \multicolumn{3}{|c}{Xeon Phi 7250 (KNL)} \\
    & \multicolumn{1}{|c}{Cores} & Time (s) & Eff. (\%) 
    & \multicolumn{1}{|c}{Cores} & Time (s) & Eff. (\%) 
    & \multicolumn{1}{|c}{Cores} & Time (s) & Eff. (\%)\\
    \hline
    1 & 16 & 300 & - & 32 & 227 & - & 64 & 193 & - \\
    2 & 32 & 150 & 100 & 64 & 108 & 105 & 128 & 92.4 & 104 \\
    4 & 64 & 72.0 & 104 & 128 & 57.3 & 99.0 & 256 & 47.3 & 102 \\
    8 & 128 & 37.9 & 98.9 & 256 & 28.7 & 98.9 & 512 & 25.2 & 95.7 \\
    16 & 256 & 18.9 & 99.2 & 512 & 13.9 & 100 & 1024 & 15.1 & 79.9\\
    32 & 512 & 9.65 & 97.2 & 1024 & 8.11 & 87.5 & 2048 & 10.6 & 56.9 \\
    64 & 1024 & 6.75 & 69.4 & 2048 &4.62 & 76.8 & 4096 & 9.27 & 32.5 \\
    \hline
  \end{tabular}}
\end{table}
%---------------------;
%  Table: Icesheet 2  ;
%---------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Hydrostatic ice sheet flow for multiple nodes: Mesh information and 
  number of solver iterations needed for an initial 128$\times$128$\times$12 coarse grid.
  KSP and SNES iteration counts may vary depending
  on the number of MPI processes used.
  \label{Tab:icesheet2}}
  \begin{tabular}{ccccc}
    \hline
    Levels of refinement & Degrees-of-freedom & SNES iterations & KSP iterations \\
    \hline
    1 & 3,014,656 & 8 & 85 \\
    2 & 23,592,960 & 8 & 85 \\
    3 & 186,646,528 & 8 & 85 \\
    4 & 1,484,783,616 & 8 & 85 \\
    5 & 11,844,714,496 & 8 & 85  \\
    \hline
  \end{tabular}
\end{table}

Table \ref{Tab:icesheet_strong} contains the strong-scaling results
for the HPC systems containing the Intel Xeon E5-2695v2 (Ivybridge), Intel Xeon E5-2698v3 
(Haswell), and Intel Xeon Phi 7250 (KNL) nodes. All three systems demonstrate near
perfect strong-scaling performance until 1024 cores are used (roughly
20k degrees-of-freedom per core). However, it is difficult to make
performance comparisons because different systems employ different
numbers of MPI processes per node which affect communication to computation
ratios as well as required data bandwidth between nodes. The only concrete conclusion 
that can be made is that the KNL system takes the least amount of 
wall-clock time on a single compute node but gets outperformed when the problem
size per node reduces. Figures \ref{Fig:iceflow1_intensity} and 
\ref{Fig:iceflow1_rate} suggest that when the problem size on a KNL node
is sufficiently small, parallel performance would degrade drastically, which
is exactly what the results of Table \ref{Tab:icesheet_strong} portray.
\subsection{Example \#1: 1024 MPI processes}
%-----------------------------------;
%  Figure: Large iceflow intensity  ;
%-----------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow2_intensity.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow multiple nodes: AI$_{L1}$ when the systems all employ 
1024 cores (64 Ivybridge nodes, 32 Haswell nodes, 
and 16 KNL nodes). Grid sizes ranging from 3 million to 
186 million degrees-of-freedom are considered.}
\label{Fig:iceflow2_intensity}
\end{figure}

%------------------------------;
%  Figure: Large iceflow rate  ;
%------------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow2_rate.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow multiple nodes: Static-scaling when 
the systems all employ 1024 MPI processes (64 Ivybridge nodes, 32 Haswell 
nodes, and 16 KNL nodes). Three levels of refinement are considered.}
\label{Fig:iceflow2_rate}
\end{figure}
%-----------------------------------;
%  Figure: Larger iceflow intensity ;
%-----------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow3_intensity.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow multiple nodes: AI$_{L1}$ when the systems all employ 
256 nodes (4096, 8192, and 16384 MPI processes for Ivybridge, Haswell, and KNL 
respectively). Five levels of refinement are considered.}
\label{Fig:iceflow3_intensity}
\end{figure}
In this section, we consider what happens when we employ the same MPI concurrency. 
This second example aims to model the performance when the same hydrostatic
ice sheet flow problem is solved utilizing 1024 MPI processes on different systems. 
We set this problem up by allocating 64 Ivybridge nodes, 32 Haswell nodes, and 16 
KNL nodes. An even larger initial 128$\times$128$\times$12 
coarse grid is selected, and we refine the problem 1--3 times. 
Table \ref{Tab:icesheet2} presents the
problem size as well as the number of nonlinear and linear solver
iterations needed for every level of refinement. Figures \ref{Fig:iceflow2_intensity} 
and \ref{Fig:iceflow2_rate} contain the intensity and rate metrics, respectively. 
The AI data points are either relatively flat or do not experience drastic 
changes upon mesh refinement. The static-scaling plot tells us that the 
Ivybridge system has the best performance as the problem gets 
larger. This behavior may seem to contradict the findings of 
the static-scaling plot in Figure \ref{Fig:iceflow1_rate},
but it is important to realize that this PETSc application is limited by
the memory-bandwidth and not the TPP for the FLOP rate. The HPC
system with Ivybridge processors has the best performance simply
because it employs more compute nodes thus more available memory. 
\subsection{Example \#2: 256 compute nodes}

%------------------------------;
%  Figure: Larger iceflow rate ;
%------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Iceflow3_rate.pdf}}
\captionsetup{format=hang}
\caption{Hydrostatic ice sheet flow multiple nodes: Static-scaling when 
the systems all employ 256 nodes (4096, 8192, and 16384 MPI processes 
for Ivybridge, Haswell, and KNL respectively). Five levels of refinement are
considered.}
\label{Fig:iceflow3_rate}
\end{figure}
The previous example is an elegant demonstration of why comparing
HPC machines based on equal MPI concurrency can produce misleading 
performance metrics, especially for computational frameworks that are
limited by the memory-bandwidth. What happens if every system employs 
the same number of compute nodes? In this third example, the HPC systems 
shall now allocate 256 compute nodes each. Thus, 
Ivybridge will use 4096 MPI processes (16 out of 24 cores per node), 
Haswell will use 8192 MPI processes (32 out of 32 cores per node), 
and KNL will use 16384 processes (64 out of 68 cores per node). We use the 
same initial coarse grid as in the previous example but now refine 
the problem 1--5 times. The AI in Figure 
\ref{Fig:iceflow3_intensity} again indicate relative consistency for
finer problems, and we again observe that the AI metric will 
drop significantly if a problem is not large enough. This 
trend corroborates the notion that the AI dropping for small
problems happens regardless of whether a single node or multiple nodes
are used. The static-scaling plot shown in Figure \ref{Fig:iceflow3_rate}
demonstrates that the Ivybridge processor does not beat out the 
Haswell processor. What's particularly interesting is that the performance
for the KNL processor drastically varies with problem size. KNL cannot
beat out Ivybridge for small problems, but KNL will beat both Ivybridge and Haswell
when a problem is neither too small nor too large. 

The performance spectrum model is useful for understanding performance characteristics 
across a wide variety of hardware architectures. Although the STREAM Triad
measurements from Table \ref{Tab:S2_HPC} suggest that KNL should greatly outperform
Ivybridge and Haswell for memory-bandwidth dominated applications, the performance
spectrum indicates that current and practical implementations of 
scientific software like PETSc v3.7.4 on KNL may be slow if the 
problem is dominated by main memory bandwidth. 
Different platforms require different implementation methodologies in order to
maximize performance, so optimizing computational frameworks to fully 
leverage the power of the KNL processor is still an open 
research problem. Nonetheless, the performance spectrum model
is useful for testing various implementations of PDE solvers 
and can be utilized to understand hardware architectures trends 
and algorithms of the future. 
