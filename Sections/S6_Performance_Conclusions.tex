%***************************************************;
%                                                   ;
%  Name                                             ;
%    S6_Performance_Conclusions.tex                 ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{CONCLUDING REMARKS}
\label{Sec:Conclusions}
In this paper, we have proposed a performance model,
referred to as the \emph{performance spectrum}, designed
to simultaneously model both the hardware/architectural and algorithmic 
efficiencies of a variety of parallel PDE solvers. The techniques
needed to approximate such efficiency metrics are 1) the arithmetic
intensity documenting the ratio of flops over data cache misses,
and 2) static-scaling, which scales up the problem while fixing 
the concurrency. This spectrum enabled us to visualize and enrich 
our current understanding of 
performance issues related to hardware limitations, software/solver 
implementation, and numerical discretization of some popular and 
state-of-the-art finite element simulation packages and solvers. 
Moreover, it has been shown that this spectrum is also useful for 
understanding performance and scalability of complex solvers and 
PDEs for nonlinear problems like hydrostatic ice sheet flow in a 
large-scale environment. Computational scientists have designed
and are still designing software and algorithms needed to answer
many of today's pressing scientific problems, so not only do we
need to solve these problems accurately but also to solve them fast.
In order to understand how fast these solvers and software are, 
particularly ones that are either black-box or designed by others,
we need a performance model, such as the proposed performance spectrum,
to help answer any questions regarding computational performance.
\subsection{Potential extension of this work}
The performance spectrum model has been proven to be a robust and 
versatile tool for modeling the performance and scalability of 
several parallel finite element solvers and packages, but there are
still plenty of research endeavors relating to this work that need
to be addressed. We briefly provide a list of some potential
future tasks and avenues of extension:
\begin{enumerate}[leftmargin=*]
\item Most of the systems chosen for this study are from Intel and have
similar performance characteristics, but processors from other vendors such 
as IBM's POWER8 \citep{IBM_Power8}, ARM-based systems \citep{Rajovic2014322},
may tell a different story. A logical extension to this work could be to 
extend this performance spectrum analysis onto GPUs. HPC architecture is constantly 
evolving, and simple benchmarks and measurements like STREAM Triad may not 
be sufficient for understanding the performance of complex solvers or algorithms 
on these state-of-the-art machines. 
\item The intensity equations presented in Section \ref{Sec:Model} are relatively
easy to incorporate into any code, but they only provide relative comparisons 
between various flavors of hardware and software. An improved methodology for 
documenting the [Work] and [TBT] metrics would certain improve the predictive
capabilities of the performance spectrum model. Other popular techniques such
as using Intel's SDE and VTune libraries can be used to measure AI on CPUs.
\item Certain PDEs, like the advection-diffusion equation, are notoriously difficult
to solve especially for high P\'eclet numbers. The performance spectrum can be useful 
to compare not only various numerical discretization (e.g., finite element vs
finite volume methods) but also special solvers when physical properties such as
velocity and diffusivity vary with length scale. Moreover, complex PDEs may also involve
multiple physical components, thus requiring mixed formulations or hybridization
techniques that may not be so trivial to solve or implement.
\item Finally, the performance spectrum is not restricted to solving PDEs. One 
can examine any computational scientific problem where performance and scalability
is limited by problem size. Examples include but are not limited to: boundary integral
equations, uncertainty quantification, parameter estimation, adaptive mesh refinement,
and parallel domain decomposition. 
\end{enumerate}

