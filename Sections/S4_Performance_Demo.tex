%***************************************************;
%                                                   ;
%  Name                                             ;
%    S4_Performance_Demonstration.tex               ;
%                                                   ;
%  Written By                                       ;
%    Justin Chang                                   ;
%    Kalyana Babu Nakshatrala                       ;
%    Matthew Knepley                                ;
%    Lennart Johnsson                               ;
%                                                   ;
%***************************************************;
\section{DEMONSTRATION OF THE PERFORMANCE SPECTRUM}
\label{Sec:Demo}
As proof-of-concept, we apply the proposed performance spectrum 
to study the computational performance of a couple of popular finite 
element packages when used to solve the steady-state diffusion 
equation. A series of demonstrations shall enrich our current 
understanding of how hardware limitations, software implementation, 
numerical discretization, and material properties can impact the 
performance and scalability. We restrict our studies to the C++ 
implementation of the FEniCS Project and the Python implementation 
of the Firedrake Project, both of which leverage several 
scientific libraries and solvers such as PETSc, HYPRE, and Trilinos/ML 
solvers. The GMRES iterative solver is used with various 
algebraic multigrid solvers set to a relative convergence tolerance 
of $10^{-7}$. All numerical simulations
are performed on a single AMD Opteron 2354 and 
Intel Xeon E5-2680v2 node as described in 
Table \ref{Tab:S2_HPC}. In this section, the performance
spectrum model is used only to assess the assembly and solve steps.

The steady-diffusion equation gives rise to a second-order 
elliptic partial differential equation. To this end, 
let $\Omega$ denote the computational domain, and let 
$\partial \Omega$ denote its boundary. A spatial point 
is denoted by $\mathbf{x}$. The unit outward normal to 
the boundary is denoted by $\widehat{\mathbf{n}} (\mathbf{x})$. 
The boundary is divided into two parts: $\Gamma^{\mathrm{D}}$ 
and $\Gamma^{\mathrm{N}}$. The part of the boundary on which
Dirichlet boundary conditions are prescribed is denoted
by $\Gamma^{\mathrm{D}}$, and the part of the boundary on
which Neumann boundary conditions are prescribed is
denoted by $\Gamma^{\mathrm{N}}$. For mathematical well-posedness
we assume that
%-----------------------------------------;
%  Equation: Mathematical well-posedness  ;
%-----------------------------------------;
\begin{align}
  \Gamma^{\mathrm{D}} \cup \Gamma^{\mathrm{N}} = \partial \Omega
  \quad \mathrm{and} \quad
  \Gamma^{\mathrm{D}} \cap \Gamma^{\mathrm{N}} = \emptyset.
\end{align}
%
The corresponding boundary value problem
takes the following form
%------------------------------------;
%  Equation: Steady-state diffusion  ;
%------------------------------------;
\begin{subequations}
  \label{Eqn:Steady_diffusion}
  \begin{alignat}{2}
    -&\mathrm{div}
    [\mathbf{D}(\mathbf{x})\mathrm{grad}[c(\mathbf{x})]] = f(\mathbf{x})
    &&\quad \mathrm{in} \; \Omega, \\
    %
    &c(\mathbf{x}) = c^{\mathrm{p}}(\mathbf{x})
    &&\quad \mathrm{on} \; \Gamma^{\mathrm{D}},\quad\mathrm{and} \\
    %
    -&\widehat{\mathbf{n}}(\mathbf{x}) \cdot \mathbf{D}(\mathbf{x})
    \mathrm{grad}[c(\mathbf{x})] = q^{\mathrm{p}}(\mathbf{x})
    &&\quad \mathrm{on} \; \Gamma^{\mathrm{N}},
  \end{alignat}
\end{subequations}
%-------------------------------------------;
%  Figure: Demo solution and meshes         ;
%-------------------------------------------;
\begin{figure}[t]
\centering
\subfloat[Analytical solution]{\includegraphics[scale=0.42]{Figures/demo_solution.png}}
\subfloat[Mesh]{\includegraphics[scale=0.42]{Figures/demo_tets.png}}
\captionsetup{format=hang}
\caption{Analytical solution of the steady-state diffusion example and the corresponding
mesh skeleton of the structure grid containing tetrahedra.}
\label{Fig:demo_solutions}
\end{figure}
%----------------------;
%  Table: Demo 1 mesh  ;
%----------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Mesh discretization and CG1 $L_2$ error norm with respect to 
  $h$-refinement.
  \label{Tab:mesh}}
  \begin{tabular}{lcccc}
    \hline
    $h$-size & tetrahedra & Vertices & FEniCS $L_2$ error & Firedrake $L_2$ error\\
    \hline
    1/20 & 48,000 & 9,261 & 1.48E-02 & 2.96E-02  \\
    1/40 & 384,000 & 68,921 & 3.90E-03 & 7.77E-03 \\
    1/60 & 1,296,000 & 226,981 & 1.75E-03 & 3.51E-03 \\
    1/80 & 3,072,000 & 531,441 & 9.89E-04 & 1.99E-03 \\
    1/100 & 6,000,000 & 1,010,301 & 6.34E-04 & 1.28E-03 \\
    1/120 & 10,368,000 & 1,771,561 & 4.41E-04 & 8.88E-04 \\
    1/140 & 16,464,000 & 2,803,221 & 3.24E-04 & 6.52E-04 \\
    \hline
    & & & slope: 1.97 & slope: 1.96
  \end{tabular}
\end{table}
where $c(\mathbf{x})$ is the scalar concentration field, $\mathbf{D}(\mathbf{x})$ 
is the diffusivity coefficient, $f(\mathbf{x})$ is the volumetric source, 
$c^{\mathrm{p}}(\mathbf{x})$ is the prescribed concentration on the boundary, 
and $q^{\mathrm{p}}(\mathbf{x})$ is the prescribed flux on the boundary. 
Assuming $\mathbf{D}(\mathbf{x}) = \mathbf{I}$, we consider the 
following analytical solution and corresponding forcing function on a unit cube:
%----------------------------------;
%  Equation: Diffusion analytical  ;
%----------------------------------;
\begin{align}
  \label{Eqn:Analytical}
  &c(\mathbf{x}) = \mathrm{sin}(2\pi x)\mathrm{sin}(2\pi y)\mathrm{sin}(2\pi z)\quad\mathrm{and}\\
  &f(\mathbf{x}) = 12\pi^2\mathrm{sin}(2\pi x)\mathrm{sin}(2\pi y)\mathrm{sin}(2\pi z).
\end{align}
Homogeneous Dirichlet boundary conditions are applied on all faces, and the
analytical solution for $c(\mathbf{x})$ is presented in Figure 
\ref{Fig:demo_solutions}. These next few studies shall consider 
the following $h$-sizes on a structured tetrahedron mesh: 
1/20, 1/40, 1/60, 1/80, 1/100, 1/120, and 1/140. All mesh information 
and $L_2$ error norms with respect to the FEniCS and Firedrake 
implementations of the continuous Galerkin (CG1) element is 
listed in Table \ref{Tab:mesh}. 
\subsection{Demo \#1: AMD Opteron 2354 vs Intel Xeon E5-2680v2}
%-----------------------------;
%  Figure: Demo 1 spectrum    ;
%-----------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo1_intensity.pdf}}
\captionsetup{format=hang}
\caption{Demo \#1: L1 arithmetic intensity for the FEniCS finite element package with 
PETSc's algebraic multigrid solver on a single Opteron 2354 and 
E5-2680v2 compute node.}
\label{Fig:demo1_intensity}
\end{figure}
%-----------------------------;
%  Figure: Demo 1 rate        ;
%-----------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo1_rate.pdf}}
\captionsetup{format=hang}
\caption{Demo \#1: Static-scaling for the FEniCS finite element package with PETSc's 
algebraic multigrid solver on a single Opteron 2354 and 
E5-2680v2 compute node.}
\label{Fig:demo1_rate}
\end{figure}

%---------------------------------;
%  Figure: Demo 1 rate  per proc  ;
%---------------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo1_rate2.pdf}}
\captionsetup{format=hang}
\caption{Demo \#1: Static-scaling per MPI process for the FEniCS finite element package 
with PETSc's algebraic multigrid solver on a single Opteron 2354 and E5-2680v2 
compute node.}
\label{Fig:demo1_rateperproc}
\end{figure}

%-----------------------------;
%  Figure: Demo 1 speedup     ;
%-----------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo1_speedup.pdf}}
\captionsetup{format=hang}
\caption{Demo \#1: Strong-scaling efficiency for the FEniCS finite element package with PETSc's 
algebraic multigrid solver on a single E5-2680v2 (Ivybridge) and Opteron 2354 compute node.}
\label{Fig:demo1_speedup}
\end{figure}
We first compare the AI between a single Intel Xeon E5-2680v2 
and AMD Opteron 2354 compute node for FEniCS's implementation 
of the CG1 element coupled with PETSc's algebraic multigrid 
preconditioner. The AI$_{\mbox{L1}}$, 
as seen from Figure \ref{Fig:demo1_intensity}, gradually decreases with
mesh refinement. Moreover, increasing the 
number of MPI processes also reduces the AI. The Intel processor 
has smaller L1 and L2 caches compared to the AMD processor, 
which explains why the former processor has lower AIs. It can be 
concluded that a higher AI on a different machine does not 
necessarily translate to better performance because clock rates
and memory bandwidths differ. The fact that differences in 
AI$_{\mbox{L1}}$ does not directly relate to time-to-solution
can be seen in Figure \ref{Fig:demo1_intensity}.

The static-scaling plot is shown in Figure \ref{Fig:demo1_rate}. It is clear
that the Intel processor is capable of solving more degrees of freedom per 
second than the AMD processor. Increasing the number of MPI processes 
improves the Rate$_{1}$ metric, which is expected 
since time to solution is amortized. Employing Rate$_{3}$ from equation 
\eqref{Eqn:S3_rate_mpi}, as seen in Figure 
\ref{Fig:demo1_rateperproc} gives us a better insight into the effect adding
more MPI processes onto a single node has on the static-scaling performance. 
We also note that when only one or two MPI processes are used, 
the degrees-of-freedom solved per second metric degrades as the 
problem size increases. We also observe that the line plots for Intel reach 
higher apexes as more MPI processes and larger problems are solved. The 
lines curves ``dipping'' to the left indicate a degradation in parallel 
performance -- the problems are very small (e.g., $h$-size of 1/20 
resulting in 9,261 degrees-of-freedom distributed among 16 MPI 
processes means each process solves roughly only 580 equations) 
thus more of the execution time is spent on interprocess communication 
and latencies than actual computation. Both the Rate$_{1}$ and Rate$_{3}$ 
lines decrease with problem size on the AMD node, whereas the line plots 
for the Intel node are relatively flat, suggesting that the FEniCS and PETSc 
combination is in fact an algorithmically scalable combination for the 
problem at hand.

Figure \ref{Fig:demo1_speedup} depicts the parallel 
efficiency on the two different
nodes. The parallel performance for the smaller
$h$-sizes is significantly worse due to the lack of computation needed for 
a given MPI concurrency. It is interesting to note that the speedup 
on the AMD node is slightly greater than on the Intel node. 
Recalling from Figure \ref{Fig:demo1_intensity} that the AI$_{\mbox{L1}}$ 
on the AMD node is larger, we can infer that higher AIs indicate 
a stronger likelihood to experience greater parallel speedup. 
This behavior is consistent with the strong-scaling 
results of the optimization-based solvers for the Chromium 
remediation problem in \citep{Chang_JOMP_2016} where
similar classes of AMD and Intel processors were experimented with.
% {\color{red}
%\begin{remark}
%We note here that we expect speedup on a single node using 
%MPI processes to be comparable to that achievable by threaded
%implementations such as OpenMP or Intel TBB, since there are 
%negligible system differences between threads and
%processes~\citep{KnepleyBrownMcInnesSmithRuppAdams2015}. 
%This kind of behavior has been observed in careful studies of
%combined MPI+OpenMP finite element simulations~\citep{TurcksinKronbichlerBangerth2016}.
%\end{remark}}
\subsection{Demo \#2: FEniCS vs Firedrake}
%-----------------------------;
%  Figure: Demo 2 intensity   ;
%-----------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo2_intensity.pdf}}
\captionsetup{format=hang}
\caption{Demo \#2: L1 arithmetic intensities for the FEniCS and Firedrake finite element packages 
with various solver packages on a single E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo2_intensity}
\end{figure}

%-----------------------------;
%  Figure: Demo 2 rate        ;
%-----------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo2_rate.pdf}}
\captionsetup{format=hang}
\caption{Demo \#2: Static-scaling for the FEniCS and Firedrake finite element packages with 
various solver packages on a single E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo2_rate}
\end{figure}

%-----------------------------;
%  Figure: Demo 2 iterations  ;
%-----------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo2_iterations.pdf}}
\captionsetup{format=hang}
\caption{Demo \#2: Number of GMRES iterations required for the FEnics and Firedrake finite element packages 
with various solver packages on a single E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo2_iterations}
\end{figure}

%--------------------------;
%  Figure: Demo 2 speedup  ;
%--------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo2_speedup.pdf}}
\captionsetup{format=hang}
\caption{Demo \#2: Correlation between the L1/L2/L3 arithmetic intensities and strong-scaling 
efficiency on a single E5-2680v2 node for up to
16 MPI processes when $h$-size = 1/140.}
\label{Fig:demo2_speedup}
\end{figure}

%------------------------;
%  Figure: Demo 2 HYPRE  ;
%------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo2_hypre.pdf}}
\captionsetup{format=hang}
\caption{Demo \#2: Comparison between HYPRE's default solver parameters and 
HYPRE's optimized solver parameters through the Firedrake package on a single 
E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo2_hypre}
\end{figure}
Next, we compare the FEniCS and Firedrake implementations of the CG1 
element with 16 MPI processes on a single Intel Xeon E5-2680v2 node. The same 
steady-state diffusion equation is considered, but we now investigate 
how other multigrid solver packages like HYPRE and ML affect the 
performance.

The AI's in Figure \ref{Fig:demo2_intensity} clearly depend on 
the software implementation, the solver used, and the problem size. 
The results in this figure suggest that the FEniCS and
Firedrake packages have very similar implementations of the 
PETSc and HYPRE multigrid solvers. However, the AI$_{\mbox{L1}}$ 
for FEniCS's implementation of the ML solver deteriorates 
rapidly with problem size. Similar behavior is observed in 
the static-scaling plot of Figure \ref{Fig:demo2_rate} where 
the data points with the highest AI also have the highest 
rate at which equations are solved. Unlike the previous 
demonstration where different hardware implementations 
were compared, the AI and rate metrics are strongly correlated to each 
other, and it is clear that FEniCS's current implementation of 
ML has some issues since the tailing off towards the right 
occurs before either the PETSc or HYPRE lines do.

With these two graphs in mind, one may wonder why the tailing off occurs. 
Does it occur due to suboptimal algorithmic convergence 
(i.e., iteration count increases with problem size), or do the 
data structures needed for important solver steps begin to drop out of cache? A scalable 
algorithm suggests that the number of solver iterations should 
not increase by much when the problem size increases, so if 
the GMRES iteration count increases significant, it is possible that 
the rate metric will decrease. Figure \ref{Fig:demo2_iterations} 
denotes the number of GMRES iterations needed for every finite element package and 
solver, and it can be seen that the iteration counts do not increase by much. These 
plots must be interpreted carefully because although the iteration 
plots may suggest algorithmic scalability, the degradation in 
AI with respect to problem size suggests that the current software 
and solver parameters are not efficiently configured to utilize 
the hardware. As shown in the previous demonstration, 
the AI is useful for predicting which algorithms will see greater 
speedups as the number of MPI processes is increased. Figure 
\ref{Fig:demo2_speedup} compares the AI$_{\mbox{L1/2/3}}$ and 
parallel performance of Firedrake's three solver implementations. 
Regardless of which level of cache is used to determine the AI, 
HYPRE and ML have the lowest and highest AI's, respectively. Moreover, 
HYPRE and ML have the worst and best parallel speedups, respectively, which 
again supports the fact that the AI metric is useful for predicting which 
algorithms may achieve the greatest parallel speedup. 

We note that the HYPRE solver has relatively bad performance, 
suggesting that the out-of-box parameters are unfit for the problem at hand.
One of the best ways to improve the AI and Rate$_{1}$ metrics is to simply adjust 
some of the solver parameters. If, for example, we optimize the parameters
by increasing the strong threshold coarsening rate, the performance improves
dramatically as we can tell from Figure \ref{Fig:demo2_hypre}. The AI and 
Rate$_{1}$ metrics are now competitive with Firedrake's implementation of the 
PETSc and ML solvers, but it is important to realize that the GMRES 
iteration counts increased with size. An algorithm that requires fewer 
iterations yet remains constant when the problem size increase
does not necessarily mean it has good performance and scalability. 
Neither the AI nor rate metrics tail off towards the right, suggesting 
that the optimized HYPRE solver is scalable despite some minor growth 
in the GMRES iteration count. As we have discussed in the previous 
demonstration, answers regarding performance and scalability 
of various solvers and software will also depend on the hardware.

\subsection{Demo \#3: Continuous Galerkin vs Discontinuous Galerkin}%----------------------;
%  Table: Demo 3 size  ;
%----------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Demo \#3: Degrees-of-freedom with respect to $h$-refinement. 
  In this study we do not consider $h$-size = 1/100 for
  the DG1 or DG2 elements. \label{Tab:demo3_size}}
  \begin{tabular}{lcccc}
    \hline
    $h$-size & CG1 & CG2 & DG1 & DG2 \\
    \hline
    1/20 & 9,261 & 68,921 & 192,000 & 480,000 \\
    1/40 & 68,921 & 531,441 & 1,536,000 & 3,840,000 \\
    1/60 & 226,981 & 1,771,561 & 5,184,000 & 12,960,000 \\
    1/80 & 531,441 & 4,173,281 & 12,288,000 & 30,720,000 \\
    1/100 & 1,030,301 & 8,120,601 & - & -\\
    \hline
  \end{tabular}
\end{table}
%------------------------;
%  Table: Demo 3 slopes  ;
%------------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Demo \#3: $L_2$ error norm with respect to $h$-refinement 
  for various finite elements provided through the Firedrake package.
  In this study we do not consider $h$-size = 1/100 for
  the DG1 or DG2 elements. \label{Tab:demo3_error}}
  \begin{tabular}{lcccc}
    \hline
    $h$-size & CG1 & CG2 & DG1 & DG2 \\
    \hline
    1/20 & 2.96E-02 & 3.81E-04 & 1.65E-02 & 2.16E-04 \\
    1/40 & 7.77E-03 & 3.79E-05 & 4.35E-03 & 2.26E-05 \\
    1/60 & 3.51E-03 & 1.06E-05 & 1.97E-03 & 6.47E-06 \\
    1/80 & 1.99E-03 & 4.44E-06 & 1.12E-03 & 2.72E-06 \\
    1/100 & 1.28E-03 & 2.25E-06 & - & -\\
    \hline
    slope: & 1.95 & 3.19 & 1.94 & 3.16
  \end{tabular}
\end{table}
%------------------------------;
%  Figure: Demo 3 intensities  ;
%------------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo3_intensities.pdf}}
\captionsetup{format=hang}
\caption{Demo \#3: L1/L2/L3 arithmetic intensities of Firedrake's 
various finite element formulations on a single E5-2680v2 node
with 16 MPI processes.}
\label{Fig:demo3_intensities}
\end{figure}

%------------------------;
%  Figure: Demo 3 rate   ;
%------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo3_rate.pdf}}
\captionsetup{format=hang}
\caption{Demo \#3: Static-scaling for Firedrake's 
various finite element formulations on a single E5-2680v2 node
with 16 MPI processes.}
\label{Fig:demo3_rate}
\end{figure}

%------------------------;
%  Figure: Demo 3 rate   ;
%------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo3_rate3.pdf}}
\captionsetup{format=hang}
\caption{Demo \#3: Degrees-of-freedom vs degrees-of-freedom solved per second 
for Firedrake's various finite element formulations on a single E5-2680v2 node
with 16 MPI processes.}
\label{Fig:demo3_rate3}
\end{figure}

%-----------------------------;
%  Figure: Demo 3 iterations  ;
%-----------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo3_iterations.pdf}}
\captionsetup{format=hang}
\caption{Demo \#3: Solver iterations needed for Firedrake's 
various finite element formulations on a single E5-2680v2 node
with 16 MPI processes.}
\label{Fig:demo3_iterations}
\end{figure}

%-----------------------------------;
%  Figure: Demo 3 rate per iterate  ;
%-----------------------------------;
\begin{figure}[t!]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo3_rate2.pdf}}
\captionsetup{format=hang}
\caption{Demo \#3: Static-scaling per solver iteration for 
Firedrake's various finite element formulations on a single 
E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo3_rateperiterate}
\end{figure}
So far we have only considered the CG1 finite element. What happens if we
employ another discretization such as the Discontinuous Galerkin (DG) method? 
Moreover, what happens if we increase the polynomial order and employ second
order CG (CG2) and second order DG (DG2) elements? Various families of 
elements and their respective levels of $p$-refinement will change both 
the size and numerical accuracy of the numerical solution, so it is
desirable to understand both the costs and benefits of these approaches
on a particular mesh. Tables \ref{Tab:demo3_size} and 
\ref{Tab:demo3_error} contain the total degrees-of-freedom and $L_2$ error norms,
respectively, of Firedrake's various finite element discretizations. The
CG elements are studied up to $h$-size = 1/100 whereas the DG elements are
studied up to $h$-size = 1/80. We again employ 16 MPI processes across 
a single Intel Xeon E5-2680v2 node, and all finite element discretizations in this 
demonstration are solved with optimized (i.e., increased strong threshold 
coarsening) HYPRE parameters. 

Figure \ref{Fig:demo3_intensities} contains the AI$_{\mbox{L1/2/3}}$ for the
CG1, CG2, DG1, and DG2 elements. What we learn from these results is that 
increasing the polynomial order for the CG elements lowers the AI whereas 
the AI increases for DG elements. This may not always be the case because 
different solvers and different hardware architectures may be better 
tailored to different discretization. Other finite element packages like the
FEniCS or DEAL.II projects may have very different results. The Rate$_{1}$ metric 
as seen from Figure \ref{Fig:demo3_rate} depicts the rate at which each 
discretization solves its equations. Alternatively, one could also compare
the Rate$_{1}$ metric with respect to the degrees-of-freedom as seen in Figure
\ref{Fig:demo3_rate3}. Although DG elements have more degrees-of-freedom
for a given mesh discretization, it is seen that the DG1 element has the highest
Rate$_{1}$ metric, suggesting that the optimized HYPRE solver parameters are 
especially suitable for DG1 elements. Unlike the FEniCS and ML combination
example from the previous demonstration, the DG2 discretization experiences 
significant degradation in the static-scaling plot yet maintains relatively
consistent AI's. This begs the question of whether the tailing off towards
the right is due to memory effects or suboptimal algorithmic convergence.

As previously observed from Figure \ref{Fig:demo2_hypre}, 
the optimized HYPRE parameters resulted in a slight increase 
in GMRES iteration count for CG1 elements, and we notice similar
trends for the other finite elements in Figure \ref{Fig:demo3_iterations}. If the
iteration count increase is significant enough, it could negatively affect 
static-scaling. To determine whether this solver iteration growth stymied the rate 
by which equations are solved, we can employ Rate$_{2}$ (i.e., degrees-of-freedom solved per 
second per solver iterate) from equation \eqref{Eqn:S3_rate_iterate}
as shown in Figure \ref{Fig:demo3_rateperiterate}. In this particular demonstration, 
it makes no difference as we still observe degradation with respect to problem 
size, hence suggesting that memory bandwidth and cache behavior have 
an adverse effect on the simulation. Using more
compute nodes may certainly ameliorate both the AI and rate metrics for
the DG2 element, but it should again be cautioned that comparative studies 
on the performance of numerical methods and solvers strongly depend on 
both the code implementation as well as the nature of the computing platform.

\subsection{Demo \#4: Material properties}
%------------------------;
%  Table: Demo 4 slopes  ;
%------------------------;
\begin{table}
  \centering
\captionsetup{format=hang}
  \caption{Demo \#4: $L_2$ error norm with respect to $h$-refinement for various values
  of $\alpha$ in equation \eqref{Eqn:anisotropic_diffusivity} when using the 
  Firedrake implementation of the CG1 element.
  \label{Tab:demo4_error}}
  \begin{tabular}{lcccccc}
    \hline
    $h$-size & $\alpha = 0$ & $\alpha = 1$ & $\alpha = 10$ & $\alpha = 100$ & $\alpha = 1000$ \\
    \hline
    1/20 & 1.48E-02 & 3.45E-02 & 4.83E-02 & 5.71E-02 & 5.86E-02 \\
    1/40 & 3.90E-03 & 9.31E-03 & 1.46E-02 & 1.90E-02 & 1.99E-02 \\
    1/60 & 1.75E-03 & 4.23E-03 & 6.84E-03 & 9.26E-03 & 9.76E-03 \\
    1/80 & 9.89E-04 & 2.40E-03 & 3.94E-03 & 5.43E-03 & 5.75E-03 \\
    1/100 & 6.34E-04 & 1.55E-03 & 2.55E-03 & 3.55E-03 & 3.77E-03 \\
    1/120 & 4.41E-04 & 1.07E-03 & 1.78E-03 & 2.49E-03 & 2.64E-03 \\
    1/140 & 3.24E-04 & 7.88E-04 & 1.31E-03 & 1.84E-03 & 1.96E-03 \\
    \hline
    slope: & 1.97 & 1.94 & 1.86 & 1.77 & 1.75
  \end{tabular}
\end{table}
%---------------------------;
%  Figure: Demo 4 spectrum  ;
%---------------------------;
\begin{figure}[t]
\centering
\subfloat{\includegraphics[scale=0.5]{Figures/Demo4_spectrum.pdf}}
\captionsetup{format=hang}
\caption{Demo \#4: Performance spectrum for various values of $\alpha$ in equation \eqref{Eqn:anisotropic_diffusivity} 
on a single E5-2680v2 node with 16 MPI processes.}
\label{Fig:demo4_spectrum}
\end{figure}
So far all three of our demonstrations have been conducted in a homogeneous 
domain. However, many scientific problems are often heterogeneous in nature, 
which may complicate the physics of the governing equations and may become more 
expensive to solve numerically. In \citep{Chang_CMAME_2017}, it was shown that
solving heterogeneous problems like chaotic flow resulted in suboptimal 
algorithmic convergence (i.e., the iteration counts grew with $h$-refinement), 
so our goal is to demonstrate how physical properties such a heterogeneity 
and anisotropy may skew how we interpret the performance. 
Let us now assume that we have a heterogeneous 
and anisotropic diffusivity tensor that can be expressed as follows
%------------------------------------;
%  Equation: Anisotropic diffusion   ;
%------------------------------------;
 \begin{align}
  \label{Eqn:anisotropic_diffusivity}
   &\mathbf{D}(\mathbf{x}) = \left(\begin{array}{ccc}
  \alpha(y^2+z^2)+1 & -\alpha xy & -\alpha xz \\
  -\alpha xy & \alpha(x^2+z^2)+1 & -\alpha yz \\
  -\alpha xz & -\alpha yz & \alpha(x^2+y^2)+1 
  \end{array}
  \right),
\end{align}
where $\alpha \geq 0$ is a user defined constant that controls the 
level of heterogeneity and anisotropy present in the computational domain. 
By employing the same analytical solution as equation \eqref{Eqn:Analytical}, 
the various values of $\alpha$ give rise to new forcing functions. The $L_2$ error 
norms with respect to $\alpha$ using Firedrake's CG1 elements are shown in Table
\ref{Tab:demo4_error}. Again a single Intel Xeon E5-2680v2 compute node with 
16 MPI processes is used for this study, and PETSc's multigrid solver is 
used to solve these problems.

Figure \ref{Fig:demo4_spectrum} depicts the AI, Rate$_{1}$, solver iterations, and 
Rate$_{2}$ metrics. The AI is not affected by $\alpha$ which suggests that there are
no hardware or software implementation issues, only that the Rate$_{1}$ metric tails off
as $\alpha$ is increased. We see that while the iteration growth is significant,
the Rate$_{2}$ metric is still flat for this heterogeneous and anisotropic 
steady-state diffusion problem. Thus, the primary reason that the data points 
in the static-scaling plots decrease with problem size has little to do with memory 
contention.
