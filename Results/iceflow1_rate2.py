import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots(figsize=(12,5))
t1 = np.loadtxt('data/iceflow_ma_1node_time')
t2 = np.loadtxt('data/iceflow_op_1node_time')
t3 = np.loadtxt('data/iceflow_ed_1node_time')
t4 = np.loadtxt('data/iceflow_has_1node_time')
t5 = np.loadtxt('data/iceflow_knl_1node_time')
r1 = np.loadtxt('data/iceflow_ma_1node_rate')
r2 = np.loadtxt('data/iceflow_op_1node_rate')
r3 = np.loadtxt('data/iceflow_ed_1node_rate')
r4 = np.loadtxt('data/iceflow_has_1node_rate')
r5 = np.loadtxt('data/iceflow_knl_1node_rate')
i1 = np.loadtxt('data/iceflow_ma_1node_intensity')
i2 = np.loadtxt('data/iceflow_op_1node_intensity')
i3 = np.loadtxt('data/iceflow_ed_1node_intensity')
i4 = np.loadtxt('data/iceflow_has_1node_intensity')
i5 = np.loadtxt('data/iceflow_knl_1node_intensity')
r1 = r1/8
r2 = r2/16
r3 = r3/16
r4 = r4/32
r5 = r5/64

ax.loglog(t1,r1,color='r',marker='o',linestyle='-',linewidth=2,label='Opteron 2354 - 8/8 cores')
ax.loglog(t2,r2,color='b',marker='s',linestyle='-',linewidth=2,label='E5-2680v2 - 16/20 cores')
ax.loglog(t3,r3,color='g',marker='^',linestyle='-',linewidth=2,label='E5-2695v2 - 16/24 cores')
ax.loglog(t4,r4,color='m',marker='v',linestyle='-',linewidth=2,label='E5-2698v3 - 32/32 cores')
ax.loglog(t5,r5,color='darkorange',marker='D',linestyle='-',linewidth=2,label='Xeon Phi 7250 - 64/68 cores')
ax.set_ylabel('Rate/proc (DOFs/(sec x proc))',fontsize=16)
ax.set_ylim(1e2,1e4)
ax.tick_params(axis='both',labelsize=14,which='both',direction='in')
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='best',fontsize=14)
leg.get_frame().set_edgecolor('k')
ax.set_xlabel('Time (sec)',fontsize=16)
ax.set_xlim(0.1,700)
plt.subplots_adjust(left=0.07,bottom=0.15,right=0.95,top=0.97,hspace=0.08)
plt.show()
