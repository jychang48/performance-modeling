import numpy as np
import matplotlib.pyplot as plt

fig, axarr = plt.subplots(nrows=1,ncols=2,sharex=False,figsize=(14,4))
t1 = np.array([3418.775,5132.654,5277.161,7896.729,10516.419])
t2 = np.array([3411.305,6628.650,10025.926,10538.550,10493.199])
t3 = np.array([15111.703,25943.573,32365.585,33416.583,33211.679,32576.124,39109.529,45573.553,52060.400,58722.611,65092.414])
t4 = np.array([15245.106,30007.867,52036.238,61163.065,64501.722,65507.419,66323.361,66067.277,65251.311,65029.803,64565.262])
proc1 = np.array([1,2,4,6,8])
proc2 = np.array([1,2,4,6,8,10,12,14,16,18,20])
speed1 = np.divide(t1,proc1)
speed2 = np.divide(t2,proc1)
speed3 = np.divide(t3,proc2)
speed4 = np.divide(t4,proc2)
eff1 = speed1/t1[0]*100
eff2 = speed2/t2[0]*100
eff3 = speed3/t3[0]*100
eff4 = speed4/t4[0]*100

axarr[0].plot(proc2,t3,color='blue',marker='o',label='Intel E5-2680v2: Method 1',linewidth=2)
axarr[0].plot(proc2,t4,color='red',marker='s',label='Intel E5-2680v2: Method 2',linewidth=2)
axarr[0].plot(proc1,t1,color='g',marker='^',label='AMD Opteron 2354: Method 1',linewidth=2)
axarr[0].plot(proc1,t2,color='m',marker='v',label='AMD Opteron 2354: Method 2',linewidth=2)
axarr[0].set_ylabel('Triad Bandwidth (MB/s)',fontsize=16)
axarr[0].tick_params(axis='both',labelsize=14)
axarr[0].ticklabel_format(style='sci', axis='y', scilimits=(0,0))
axarr[0].grid(b=True,which='both', linestyle=':')
#axarr[0].legend(loc='best')
axarr[0].set_xlabel('MPI Processes',fontsize=16)
axarr[0].set_xlim(1,20)
axarr[0].set_xticks((1,2,4,6,8,10,12,14,16,18,20))
axarr[1].plot(proc2,eff3,color='blue',marker='o',label='Intel E5-2680v2: Method 1',linewidth=2)
axarr[1].plot(proc2,eff4,color='red',marker='s',label='Intel E5-2680v2: Method 2',linewidth=2)
axarr[1].plot(proc1,eff1,color='g',marker='^',label='AMD Opteron 2354: Method 1',linewidth=2)
axarr[1].plot(proc1,eff2,color='m',marker='v',label='AMD Opteron 2354: Method 2',linewidth=2)
axarr[1].set_ylabel('Expected Parallel Efficiency (%)',fontsize=16)
axarr[1].tick_params(axis='both',labelsize=14)
axarr[1].grid(b=True,which='both', linestyle=':')
axarr[1].legend(loc='best')
axarr[1].set_xlabel('MPI Processes',fontsize=16)
axarr[1].set_xlim(1,20)
axarr[1].set_xticks((1,2,4,6,8,10,12,14,16,18,20))
plt.subplots_adjust(left=0.05,bottom=0.15,right=0.95,top=0.95)
plt.show()
