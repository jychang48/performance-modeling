import numpy as np
import matplotlib.pyplot as plt

fig, axarr = plt.subplots(2,sharex=True,figsize=(12,8))
t1 = np.loadtxt('data/opFEniCS_1proc_time')
t2 = np.loadtxt('data/opFEniCS_2proc_time')
t3 = np.loadtxt('data/opFEniCS_4proc_time')
t4 = np.loadtxt('data/opFEniCS_8proc_time')
t5 = np.loadtxt('data/opFEniCS_16proc_time')
t6 = np.loadtxt('data/maFEniCS_1proc_time')
t7 = np.loadtxt('data/maFEniCS_2proc_time')
t8 = np.loadtxt('data/maFEniCS_4proc_time')
t9 = np.loadtxt('data/maFEniCS_8proc_time')

opuntiafactor = np.array([0.01,0.02,0.04,0.08,0.16])
maxwellfactor = np.array([0.01,0.02,0.04,0.08])
s1 = np.divide(t1,t1)
s2 = np.divide(t1,t2)
s3 = np.divide(t1,t3)
s4 = np.divide(t1,t4)
s5 = np.divide(t1,t5)
s6 = np.divide(t6,t6)
s7 = np.divide(t6,t7)
s8 = np.divide(t6,t8)
s9 = np.divide(t6,t9)
eff1 = s1/0.01
eff2 = s2/0.02
eff3 = s3/0.04
eff4 = s4/0.08
eff5 = s5/0.16
eff6 = s6/0.01
eff7 = s7/0.02
eff8 = s8/0.04
eff9 = s9/0.08
N = 7
index = np.arange(N)

barwidth1=0.16
barwidth2=0.2
bar1 = axarr[0].bar(index+0.1,eff1,barwidth1,color='blue',label='1 MPI process')
bar2 = axarr[0].bar(index+0.1+barwidth1,eff2,barwidth1,color='darkviolet',label='2 MPI processes')
bar3 = axarr[0].bar(index+0.1+barwidth1*2,eff3,barwidth1,color='darkred',label='4 MPI processes')
bar4 = axarr[0].bar(index+0.1+barwidth1*3,eff4,barwidth1,color='orangered',label='8 MPI processes')
bar5 = axarr[0].bar(index+0.1+barwidth1*4,eff5,barwidth1,color='goldenrod',label='16 MPI processes')
axarr[0].set_ylabel('Ivybridge parallel eff. (%)',fontsize=16)
axarr[0].tick_params(axis='both',labelsize=14)
axarr[0].grid(b=True,which='both', linestyle=':')
axarr[0].legend(loc='center right',bbox_to_anchor=(1.3,0.5))
axarr[1].bar(index+barwidth2*0.5,eff6,barwidth2,color='blue',label='1 MPI process')
axarr[1].bar(index+barwidth2*1.5,eff7,barwidth2,color='darkviolet',label='2 MPI processes')
axarr[1].bar(index+barwidth2*2.5,eff8,barwidth2,color='darkred',label='4 MPI processes')
axarr[1].bar(index+barwidth2*3.5,eff9,barwidth2,color='orangered',label='8 MPI processes')
axarr[1].set_xlabel('h-size',fontsize=16)
axarr[1].set_xticks(index+0.5)
axarr[1].set_xticklabels(('1/20','1/40','1/60','1/80','1/100','1/120','1/140'))
axarr[1].set_ylabel('Opteron parallel eff. (%)',fontsize=16)
axarr[1].tick_params(axis='both',labelsize=14)
axarr[1].grid(b=True,which='both',linestyle=':')
axarr[1].legend(loc='center right',bbox_to_anchor=(1.3,0.5))
plt.subplots_adjust(left=0.07,bottom=0.07,right=0.79,top=0.95,hspace=0.1)
plt.show()
