import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots(figsize=(12,5))
t1 = np.loadtxt('data/opFEniCS_1proc_time')
i1 = np.loadtxt('data/opFEniCS_1proc_rate')
t2 = np.loadtxt('data/opFEniCS_2proc_time')
i2 = np.loadtxt('data/opFEniCS_2proc_rate')
t3 = np.loadtxt('data/opFEniCS_4proc_time')
i3 = np.loadtxt('data/opFEniCS_4proc_rate')
t4 = np.loadtxt('data/opFEniCS_8proc_time')
i4 = np.loadtxt('data/opFEniCS_8proc_rate')
t5 = np.loadtxt('data/opFEniCS_16proc_time')
i5 = np.loadtxt('data/opFEniCS_16proc_rate')
t6 = np.loadtxt('data/maFEniCS_1proc_time')
i6 = np.loadtxt('data/maFEniCS_1proc_rate')
t7 = np.loadtxt('data/maFEniCS_2proc_time')
i7 = np.loadtxt('data/maFEniCS_2proc_rate')
t8 = np.loadtxt('data/maFEniCS_4proc_time')
i8 = np.loadtxt('data/maFEniCS_4proc_rate')
t9 = np.loadtxt('data/maFEniCS_8proc_time')
i9 = np.loadtxt('data/maFEniCS_8proc_rate')
r1 = i1
r2 = i2/2
r3 = i3/4
r4 = i4/8
r5 = i5/16
r6 = i6
r7 = i7/2
r8 = i8/4
r9 = i9/8
ax.loglog(t1,r1,color='blue',marker='o',label='E5-2680v2 - 1 MPI process',linewidth=2)
ax.loglog(t2,r2,color='darkviolet',marker='s',label='E5-2680v2 - 2 MPI processes',linewidth=2)
ax.loglog(t3,r3,color='darkred',marker='^',label='E5-2680v2 - 4 MPI processes',linewidth=2)
ax.loglog(t4,r4,color='orangered',marker='v',label='E5-2680v2 - 8 MPI processes',linewidth=2)
ax.loglog(t5,r5,color='goldenrod',marker='D',label='E5-2680v2 - 16 MPI processes',linewidth=2)
ax.loglog(t6,r6,color='blue',marker='o',linestyle='--',label='Opteron 2354 - 1 MPI process',linewidth=2)
ax.loglog(t7,r7,color='darkviolet',marker='s',linestyle='--',label='Opteron 2354 - 2 MPI processes',linewidth=2)
ax.loglog(t8,r8,color='darkred',marker='^',linestyle='--',label='Opteron 2354 - 4 MPI processes',linewidth=2)
ax.loglog(t9,r9,color='orangered',marker='v',linestyle='--',label='Opteron 2354 - 8 MPI processes',linewidth=2)
ax.set_ylabel('Rate/proc (DOFs/(sec x proc))',fontsize=16)
ax.tick_params(axis='both',labelsize=14,which='both',direction='in')
ax.set_xlim(0.03,5000)
ax.set_ylim(8e3,1e5)
ax.grid(b=True,which='both', linestyle=':')
leg = ax.legend(loc='upper right',fontsize=14)
leg.get_frame().set_edgecolor('k')
ax.set_xlabel('Time (sec)',fontsize=16)
plt.subplots_adjust(left=0.07,bottom=0.15,right=0.96,top=0.95,hspace=0.1)
plt.show()
