%----------------------------------------------------;
%                                                    ;
%       Response to CPE                              ;
%       Written by:                                  ;
%         Justin Chang                               ;
%         Kalyana Nakshatrala                        ;
%         Matthew Knepley                            ;
%         Lennart Johnsson                           ;
%                                                    ;
%----------------------------------------------------;
\documentclass[11pt]{amsart}
\linespread{1.1}
\usepackage{fullpage}
\usepackage{color}
\usepackage{enumerate}
\usepackage{tcolorbox}

%======================;
%  Title of the paper  ;
%======================;
\title{A performance spectrum for parallel computational frameworks that solve PDEs}

\author{J.~Chang and K.~B.~Nakshatrala and M.~G.~Knepley and L.~Johnsson}
%
\begin{document}
\maketitle

\vspace{-0.1in} 

\emph{We thank all the reviewers for 
their comments. Please note that all corrections in the revised manuscript 
are marked in red.}

%===========================;
%  Response to reviewer #1  ;
%===========================;
\begin{center}
	\textbf{\underline{Response to Reviewer \#1}}
\end{center}

\begin{tcolorbox} \textbf{Comment \#1:}  This paper proposes a performance 
model designed to model hardware and algorithmic efficiency for PDE solvers.
The model is based on two concepts: arithmetic intensity and static-scaling.
This model helps to understand the performance issues related to hardware, 
software of the numerical method used to solve a PDE.

The document is very well written and enriched with numerical experiments 
that show the benefits of using the measures proposed by the authors. 
Thus, in my opinion, the paper can be published
in this journal without too many modifications.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We thank the reviewer for the kind words about 
our paper. 

\begin{tcolorbox} \textbf{Comment \#2:} The main drawback regards to the length of the document which is very long. I suggest the authors to
explore some way in which the length can be shorten someway (omitting some case demo, using a more concise explanation, ...).
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We agree 
that this manuscript may be of some length. 
%
We removed the discussion on ``serial vs parallel 
performance'' from Section 2, as this omission does 
not affect in conveying our central message about 
the proposed performance spectrum.
%
Each specific demo in intended to demonstrate a particular capability of the performance spectrum analysis. We have not
removed any cases but endeavored to keep them economical.

\begin{tcolorbox} \textbf{Comment \#3:} Another concern regards with 
the use in the experiments of MPI applications when using only one node.
MPI (Message Passing Interface) was conceived to distributed memory 
parallel configurations. This execution mode is questionable since there 
are better ways to seek for concurrency intranode using,
e.g. OpenMP, TBB, or other. Thus, it is missing, at least, an explanation 
about why are using MPI when using only one node in the experiments. 
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We have added some text to the end of Section 4.1 in order to explain
our usage here and given references for this in the text. In short, given that there are no overhead differences on
these systems between threads and processes, and optimized MPI implementation should show comparable performance to a
threaded implementation.

\begin{tcolorbox} \textbf{Comment \#4:} In addition, there are small issues to tackle:

1) Figure 9 really depicts efficiency not speedup so change first sentence of 
last paragraph of 4.1.

2) The AI's in 10 $->$ The AI's in Figure 10. The same for Figure 12.

3) In Eq. 4.5 the comma is missplaced.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: Thanks, we have made the suggested fixes. 

\clearpage
\newpage

%===========================;
%  Response to reviewer #2  ;
%===========================;
\begin{center}
	\textbf{\underline{Response to Reviewer \#2}}
\end{center}

\begin{tcolorbox} \textbf{Comment \#1:}  Excellent  article 
and a really good idea  to  have a performance  model 
(spectrum for comparisons) of this nature.

Paper is  well presented, with   understandable  example plots and studies. 
Conclusions seem well supported by the work and bibliography is of suitable 
breadth and depth
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We thank the reviewer for 
the kind words.

\clearpage
\newpage

%===========================;
%  Response to reviewer #3  ;
%===========================;
\begin{center}
	\textbf{\underline{Response to Reviewer \#3}}
\end{center}

\begin{tcolorbox} \textbf{Comment \#1:} This paper is both highly original and 
also somewhat flawed in its originality. There is clearly something interesting 
here but it needs a lot more work. The main idea of the paper is to propose a 
number of new metrics and to explore how well these proposed approaches work in 
the context of codes and architectures. \\

Metrics such as these should really be informative and general. The metrics 
proposed here are not without interest but are not as general as they should 
be for modern hpc architectures.\\

The paper talks about efficiency and scalability but spend perhaps too much 
time on its AI\_LX metric. The reason that this This problems is that this metric 
is geared towards traditional CPUs, and of limited use in a parallel architecture.
\end{tcolorbox}
\noindent \underline{\textbf{\emph{Response}}}: It is true that AI\_LXis useful in understanding single node
performance. For multiple nodes, our strategy is to look for degradation from perfect speedup, and thus we must
necessarily include single node performance as part of the analysis. This is part of the import of the static scaling
analysis.

\begin{tcolorbox} \textbf{Comment \#2:}  Table 1 shows a curious mix of old 
and new processors used. Why are there no GPUs in this? They are supposed to 
be the processor that KNL is competing against? In terms of domain decomposition, 
a couple of notable approaches such as Zoltan and space-filling curves are not 
described.\\

In general there is perhaps not enough emphasis on code that don't use a finite element approach and may not use
a linear solver. There are many of these in the real world
\end{tcolorbox}
\noindent \underline{\textbf{\emph{Response}}}: Our aim in this paper is to provide useful tools for performance
comparison of complex codes (rather than microbenchmarks). In order to control the complexity both of the presentation
and the size of the possible comparison space, we confine ourselves to a part of the architecture space, namely several
generations of Intel and AMD architectures. Comparison of CPUs and GPUs forms part of our upcoming work, and due to the
explanation necessary, we think it should be treated in a separate paper. We plan to address a wider array of
applications in upcoming work, and also hope that this paper is sufficiently clear that other researchers can perform
this analysis for their own application systems.  

\begin{tcolorbox} \textbf{Comment \#3:}  The statement of page 8 about 
solver convergence rather suggests that the authors do not really solve problems
at scale. Of course it is hard to solve systems at large scale, but given that 
many problems are solved over a long tome period - five to ten years is not unusual, 
the cost is amortized over a long time.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We attempted to clarify this. The authors have years of experience
solving complex, multiphysics problems on the largest parallel machines. We apologize for the confusion.

\begin{tcolorbox} \textbf{Comment \#4:} The statement on page 8 about serial 
versus parallel performance is seriously flawed. Indeed it is almost insulting
to anyone at SC who has put in for Gordon Bell, for example. The idea that good 
serial codes don't scale is nonsense. Most scaling depends on  volume to surface 
area ratio argument. providing that there is enough work in the
volume that doesn't need outside communications costs then scaling is possible. 
The reference to Amdhal's law should also include a discussion of 
Gustavson's counterexamples. Indeed there are also architectures built on the 
fast communications with slower  processors approach - BG/Q for example.
KNL too, to some extent. The idea that people go around gaming metrics rather 
suggests they have too much free time. If statements like this are made there 
has to be evidence to support them.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We agree that this bullet point 
has some flawed statements. This block of text has been removed altogether.

\begin{tcolorbox} \textbf{Comment \#5:} Section 3.1 The AI metric. It is hard 
to see how to apply this to KNL or GPUs for example. What about fast memory, mpi
or TLB misses for example? This is not a good metric except for simple single nodes.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: The AI metric is useful for the simple model, usually associated with
the roofline plot, that incorporates memory bandwidth and computation rate. The result of this paper, and of countless
other papers, show that this model is sufficient for much of scientific programming, and in particular for complex
computations with many dissimilar parts. The issue of fast local memory (cache) is only pertinent if the computation
involves significant reuse, which neither SpMV nor any of BLAS1 does. This analysis is orthogonal to MPI, as we noted in
responses above. We have been enable to find any significant examples of TLB limitations for large scientific problems,
but would be happy to incorporate any references for this.

\begin{tcolorbox} \textbf{Comment \#6:} Section 3.2\\
The rate metric is of more interest. It would be good to have more of a 
comparison against strong and weak scaling.
The rate2 metric is curious with the additional emphasis on iteration 
when these loops are already in the CPU time?
The  Rate3 metrics seems OK as the inclusion of MPI processes gives 
the total time across all nodes. This metric though :w has to be related 
to weak scaling?
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: The rate2 metric is intended to decouple the issue of algorithmic
scaling, namely the convergence of an algorithm as problem size is increased, from that of hardware scaling, as is
employed in other analyses~\cite{knollkeyes:04}.

\begin{tcolorbox} \textbf{Comment \#7:} 3.3 it would be good to see a non-finite element code here that doesn't need a linear solver so as to provide a degree
of diversity.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: The performance of an explicit time marching code would depend
principally on the BLAS1 linear algebra used to timestep, and on the particular residual evaluation. The linear algbera
portion is already captured well by our current application mix. A careful study including non-finite element codes,
such as finite volume, would indeed be a topic worthy of its own paper. This has been noted in Section 3.2 and 7.1.

\begin{tcolorbox} \textbf{Comment \#8:} Section 4.0\\

One of the problems is that the figures shown in Section 4 5 and 6  here are 
not very helpful in that a processor may have a higher intensity but run more 
slowly, if it has slower cores, as with the KNL. The rate matric seems better 
in this respect, but it is still hard ito compare the like with like runs. 
There doesn't seem to be a case in which a high rate leads tp a slower time 
so why is time even plotted?
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: It is definitely true that higher intensities do not imply faster
time-to-solution, and this is a fundamental point brought forth in this paper. Figure 3 indicates that the intensity
metric is independent of time. From these proof-of-concept results, we have learned that frameworks/solvers with higher
AI tend to have greater strong-scaling, which will affect time-to-solution when more nodes/cores are used.

Time is used as the abscissa because time-to-solution is what most application scientists care about. The rate (dofs/sec)
metric helps us understand exactly how efficient the code is, and in particular the marginal efficient as more dofs are
added. We agree that plotting dofs/sec over time may not always be the most insightful, and in Figure 17 we substitute
dofs for time in the work metric. This is particularly useful when comparing various discretizations, and could be
useful if one were to compare finite elements against non-finite elements.
 
\begin{tcolorbox} \textbf{Comment \#9:} The comparison between the 
Opteron and the Xeon  is a bit problematic. The xeon clearly wins out 
but also costs 50\% more, at least from a standard supplier as far as 
I could tell. If that fact is taken into account then the results look 
different. While it is not possible to include cost in the metrics
the difference between the processors in terms of cost should be 
considered somewhere. You get what you pay for.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: It would be interesting to compare cost, but the market for larger
machines is far from efficient. Many different subsidies and in-kind benefits are made available during purchase, which
greatly complicates the determination of a single price. We have thus avoided this issue.

\begin{tcolorbox} \textbf{Comment \#10:} If the intensity metric is 
removed then this section is reduced in length. That would be a good 
thing. Particularly as this metric is not of use in the multi-node 
case.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We have tried to address the salience of the intensity metric to
multinode analysis.

\begin{tcolorbox} \textbf{Comment \#11:} Section 5.0
Why not run tests in which the more modern architectures are saturated? 
I don't understand this? Is the opteron even important any more?
What does the lack of saturation do to the results in Figures 22-24?
Well of course KNL gets less perrormance per core than the Xeons! 
Its cores are slower after all.\\

Section 6.0
Why are such tiny rtuns done ion the multiple nodes case? Section 6.2 is better.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: The objective of these two sections is to understand the performance
spectrum analysis for a wide range of problem sizes. The ``tiny'' problem sizes in these two Sections were chosen to be
under saturated in order to depict strong-scaling effects. Section 6.2 had larger problem sizes in order to depict
weak-scaling  effects. Strong scaling is very important for many scientific computations, for example atmospheric
simulations with tightly coupled column physics.

\begin{tcolorbox} \textbf{Comment \#12:} Section 7.0
What is really missing here is a GPU. Without this the study is not as 
meaningful as it might be.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: The purpose of this paper is to 
present a performance spectrum model which is capable of characterizing both 
hardware and algorithmic efficiency. Adding GPU results would not change the fundamentals
of the performance spectrum analysis itself, but is certainly a topic worthy of exploring
in the future. We have made a note in Section 7.1 about this possible research endeavor.
\clearpage
\newpage

%===========================;
%  Response to reviewer #4  ;
%===========================;
\begin{center}
	\textbf{\underline{Response to Reviewer \#4}}
\end{center}

\begin{tcolorbox} \textbf{Comment \#1:} 
The article is well written, definitons of performance spectrum 
is well documented although the main reference (Brown 2016) is 
a private communication and the referee has no access. 
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: We thank the reviewer for 
the kind words. We have inserted a footnote regarding the origin of the term
\emph{performance spectrum}.

\begin{tcolorbox} \textbf{Comment \#2:} 
Also, there are enough examples showing the new technique. 
However, there is lack of refererences and comparisons to 
other performance tools (Intel Performance Tuning Utility, 
Scalasca, TAU, HPCToolkit, Score-P, etc.), which are well 
documented and it will show how better it is.  I ask for 
comparisons with other performance tools. 
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: Those are all excellent performance
tools which can help understand performance from the hardware point of view, and we
have now included additional references in Section 1. Our aim is to augment the 
information provided by tools like TAU and HPCToolkit with a new analysis in order 
to help users understand algorithmic scalability, which is at the heart of
the static-scaling metric. It is also possible to use 
the output of Intel's SDE and VTune in order to calculate arithmetic
intensity, and we have inserted a remark about it in Section 3 as well as 
in Section 7.1. It is our hope that this analysis
will eventually be incorporated into performance tools such as these.

\begin{tcolorbox} \textbf{Comment \#3:} 
Also, some guides about how to improve programming from the data (programming 
techniques) will help users to incude this tool in their work.
\end{tcolorbox}

\noindent \underline{\textbf{\emph{Response}}}: This is a laudable goal and our eventual aim. Given the great variety of operating systems,
programming languages, and libraries, general advice for efficient coding is difficult to formulate, and we do not yet
have a clear idea how to formulate it. In this paper, we try to provide tools for diagnosing performance problems, which
users will hopefully employ to optimize code for their particular system.

\bibliographystyle{plainnat}
\bibliography{../../references}

\end{document}
